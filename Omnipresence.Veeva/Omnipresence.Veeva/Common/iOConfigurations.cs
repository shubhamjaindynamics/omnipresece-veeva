﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Veeva
{
    internal class iOConfigurations
    {

        internal const string OrganizationURL = "Organization URL";

        internal const string System = "SYSTEM";

        internal struct CallPlansPendingApprovalNotification
        {
            internal const string FetchXMLOfTargetRecords = "CallPlansPendingApprovalNotification.FetchXMLOfTargetRecords";
            internal const string FetchXmlOfEmailTemplate = "CallPlansPendingApprovalNotification.FetchXmlOfEmailTemplate";
            internal const string TriggerTimeZoneName = "CallPlansPendingApprovalNotification.TriggerTimeZoneName";
            internal const string MyTeamViewName = "CallPlansPendingApprovalNotification.Tags.MyTeamOpenCallPlansName";
            internal const string MyTeamViewUrl = "CallPlansPendingApprovalNotification.Tags.MyTeamOpenCallPlansView";
            internal const string EmailTemplate = "CallPlansPendingApprovalNotification.EmailTemplate";

            internal struct Tags
            {
                internal const string CallPlansCount = "{call_plans_count}";
                internal const string ClickHere = "{Click_here_to_review}";
            }
        }
        internal struct Approver
        {
            internal const string DuplicateApprover = "Approver_PostOperation.DuplicateApproverError";
        }

        internal struct ApprovalConfigurations
        {
            internal const string ContactCreation = "ApprovalConfiguration.CustomerCreation";
            internal const string AccountCreation =   "ApprovalConfiguration.AccountCreation";
        }

        internal struct DuplicateRecordWarning
        {
            public const string Message = "DuplicateRecordWarning.Message";
            public const string UniqueAttributesTag = "DuplicateRecordWarning.UniqueAttributesTag";

            internal struct UniqueAttributes
            {
                public const string indskr_customerreachfrequency = "DuplicateRecordWarning.UniqueAttributes.indskr_customerreachfrequency";
                public const string indskr_segmentreachfrequency = "DuplicateRecordWarning.UniqueAttributes.indskr_segmentreachfrequency";
                public const string indskr_contenttoken = "DuplicateRecordWarning.UniqueAttributes.indskr_contenttoken";
                public const string indskr_customerposition = "DuplicateRecordWarning.UniqueAttributes.indskr_customerposition";
                public const string indskr_email_address = "DuplicateRecordWarning.UniqueAttributes.indskr_email_address";
                public const string indskr_wechatfollow = "DuplicateRecordWarning.UniqueAttributes.indskr_wechatfollow";
                public const string indskr_wechatfollowactivity = "DuplicateRecordWarning.UniqueAttributes.indskr_wechatfollowactivity";
                public const string indskr_rec_engagement_period = "DuplicateRecordWarning.UniqueAttributes.indskr_rec_engagement_period";
                public const string indskr_lu_city = "DuplicateRecordWarning.UniqueAttributes.indskr_lu_city";
                public const string indskr_lu_postalcode = "DuplicateRecordWarning.UniqueAttributes.indskr_lu_postalcode";
                public const string indskr_address = "DuplicateRecordWarning.UniqueAttributes.indskr_address";
                public const string indskr_activitytype = "DuplicateRecordWarning.UniqueAttributes.indskr_activitytype";
                public const string indskr_activitysubtype = "DuplicateRecordWarning.UniqueAttributes.indskr_activitysubtype";
                public const string indskr_measureboardkpi = "DuplicateRecordWarning.UniqueAttributes.indskr_measureboardkpi";
            }

            internal struct Messages
            {
                public const string indskr_activityaccount = "DuplicateRecordWarning.Message.indskr_activityaccount";
                public const string indskr_content_token_value = "DuplicateRecordWarning.Message.indskr_content_token_value";
                public const string indskr_userposition = "DuplicateRecordWarning.Message.indskr_userposition";
                public const string indskr_productassignment = "DuplicateRecordWarning.Message.indskr_productassignment";
                public const string indskr_productrating = "DuplicateRecordWarning.Message.indskr_productrating";
                public const string indskr_activityproduct = "DuplicateRecordWarning.Message.indskr_activityproduct";
                public const string indskr_activitycontact = "DuplicateRecordWarning.Message.indskr_activitycontact";
                public const string indskr_customeravailability_v2 = "DuplicateRecordWarning.Message.indskr_customeravailability_v2";
                public const string indskr_indskr_customeraddress_v2 = "DuplicateRecordWarning.Message.indskr_indskr_customeraddress_v2";
                public const string indskr_lu_area = "DuplicateRecordWarning.Message.indskr_lu_area";
                public const string indskr_lu_district = "DuplicateRecordWarning.Message.indskr_lu_district";
                public const string indskr_accountcontactaffiliation = "DuplicateRecordWarning.Message.indskr_accountcontactaffiliation";
                public const string indskr_routingconfigurationrule = "DuplicateRecordWarning.Message.indskr_routingconfigurationrule";
                public const string indskr_usercustomskill = "DuplicateRecordWarning.Message.indskr_usercustomskill";
                public const string indskr_accompanieduser = "DuplicateRecordWarning.Message.indskr_accompanieduser";
                public const string indskr_asmtcategorybuassociation = "DuplicateRecordWarning.Message.indskr_asmtcategorybuassociation";
                public const string indskr_assessmentmetrics = "DuplicateRecordWarning.Message.indskr_assessmentmetrics";
                public const string indskr_coachingmeeting = "DuplicateRecordWarning.Message.indskr_coachingmeeting";
                public const string indskr_coachingreport = "DuplicateRecordWarning.Message.indskr_coachingreport";
                public const string indskr_timeoffrequest = "DuplicateRecordWarning.Message.indskr_timeoffrequest";
                public const string indskr_customersegmentation = "DuplicateRecordWarning.Message.indskr_customersegmentation";
                public const string indskr_processtrigger = "DuplicateRecordWarning.Message.indskr_processtrigger";
                public const string indskr_coachingtemplatemeasure = "DuplicateRecordWarning.Message.indskr_coachingtemplatemeasure";
                public const string indskr_customersampleproduct = "DuplicateRecordWarning.Message.indskr_customersampleproduct";
                public const string indskr_customercontentmatch = "DuplicateRecordWarning.Message.indskr_customercontentmatch";
                public const string indskr_matchedrulegroup = "DuplicateRecordWarning.Message.indskr_matchedrulegroup";
                public const string indskr_activitytherapeuticarea = "DuplicateRecordWarning.Message.indskr_activitytherapeuticarea";
                public const string indskr_callreason = "DuplicateRecordWarning.Message.indskr_callreason";
                public const string indskr_project = "DuplicateRecordWarning.Message.indskr_project";
                public const string indskr_disposition = "DuplicateRecordWarning.Message.indskr_disposition";
                public const string indskr_accountbrandaffiliation = "DuplicateRecordWarning.Message.indskr_accountbrandaffiliation";
                public const string indskr_leadaddress = "DuplicateRecordWarning.Message.indskr_leadaddress";
                public const string indskr_customerlicense = "DuplicateRecordWarning.Message.indskr_customerlicense";
                public const string indskr_projectcallreason = "DuplicateRecordWarning.Message.indskr_projectcallreason";
                public const string indskr_callobjective = "DuplicateRecordWarning.Message.indskr_callobjective";
                public const string indskr_searchentity = "DuplicateRecordWarning.Message.indskr_searchentity";
                public const string indskr_businessunitapprovalconfiguration = "DuplicateRecordWarning.Message.indskr_businessunitapprovalconfiguration";
                public const string indskr_expertengagementplan = "DuplicateRecordWarning.Message.indskr_expertengagementplan";
                public const string indskr_measureboardkpi = "DuplicateRecordWarning.Message.indskr_measureboardkpi";
            }
        }

        internal struct RegularExpression
        {
            internal const string EmailSubject = "<s*emailsubject[^>]*>(.*?)<s*/s*emailsubject>";
            internal const string Tags = "<[^>]*>";
            internal const string ReplaceWhiteSpace = "Regex.AlternateNames";
        }

        internal struct CustomerAvailability_PreOperation
        {
            internal const string InvalidTimeWarning = "CustomerAvailability_PreOperation.InvalidTimeWarning";
        }

        internal struct EmailFields
        {
            internal const string Fields = "Email.SendIndividuallyFields";
        }

        internal struct Country_PostOperation
        {
            internal const string FetchXML_ContactsList = "Country_PostOperation.FetchXML_ContactsList";
            internal const string CountryGUIDTag = "[[COUNTRY_GUID]]";
        }

        internal struct State_PostOperation
        {
            internal const string FetchXML_ContactsList = "State_PostOperation.FetchXML_ContactsList";
            internal const string StateGUIDTag = "[[STATE_GUID]]";
        }

        internal struct City_PostOperation
        {
            internal const string FetchXML_ContactsList = "City_PostOperation.FetchXML_ContactsList";
            internal const string CityGUIDTag = "[[CITY_GUID]]";
        }

        internal struct PostalCode_PostOperation
        {
            internal const string FetchXML_ContactsList = "PostalCode_PostOperation.FetchXML_ContactsList";
            internal const string FetchXML_AddressesList = "PostalCode_PostOperation.FetchXML_AddressesList";
            internal const string PostalCodeGUIDTag = "[[POSTALCODE_GUID]]";
        }

        internal struct Appointment_PreOperation
        {
            internal struct Messages
            {
                internal const string MeetingCompletion_MissingAttendies = "Appointment_PreOperation.Messages.MeetingCompletion.MissingAttendies";
                internal const string MeetingCompletion_MissingProducts = "Appointment_PreOperation.Messages.MeetingCompletion.MissingProducts";
            }
        }

        internal struct Entities
        {
            internal struct ConsentActivities
            {
                internal const string OptInPrefix = "Entities.ConsentActivities.OptInPrefix";
                internal const string OptOutPrefix = "Entities.ConsentActivities.OptOutPrefix";
            }
        }

        internal struct ConsentTerms_PostOperation
        {
            internal struct ExpirationTrigger
            {
                internal const string ProcessType = "ConsentTerms_PostOperation.ExpirationTrigger.ProcessType";
                internal const string ProcessLogicalName = "ConsentTerms_PostOperation.ExpirationTrigger.ProcessLogicalName";
                internal const string ProcessArguments = "ConsentTerms_PostOperation.ExpirationTrigger.ProcessArguments";
            }

            internal struct Messages
            {
                internal const string ExpirationExtension = "ConsentTerms_PostOperation.Messages.ExpirationExtension";
                internal const string FromDateGreaterThanExpiration = "ConsentTerms_PostOperation.Messages.FromDateGreaterThanExpiration";
                internal const string CannotDelete = "ConsentTerms_PostOperation.Messages.CannotDelete";
            }


        }

        internal struct SegmentCallPlan_PreOperation
        {
            internal const string RecordMissingMessage = "SegmentCallPlan_PreOperation.RecordMissingMessage";
        }

        internal struct OrganizationSetting_PreOperation
        {
            internal const string ActiveRecordsMessage = "OrganizationSetting_PreOperation.ActiveRecordsMessage";
        }

        internal struct AssessmentRatingScale_PreOperation
        {
            internal const string ActiveRecordsMessage = "AssessmentRatingScale_PreOperation.ActiveRecordsMessage";
        }
        internal struct SampleRequest_PreOperation
        {
            internal const string ReadOnlyErrorMessage = "SampleRequest_PreOperation.ReadOnlyErrorMessage";
            internal const string ReopeningErrorMessage = "SampleRequest_PreOperation.CompletedSampleDropReOpening";
        }

        internal struct AllocationShipment_Action
        {
            internal const string AllocationShipmentAndRecallLogicAppUrl = "AllocationShipmentAndRecallLogicAppUrl";
            internal const string AllocationShipmentRedistributeLogicAppUrl = "AllocationShipmentRedistributeLogicAppUrl";
        }

        internal struct SurveyDistibution_Action
        {
            internal const string SurveyDistributionLogicalAppUrl = "SurveyDistributionLogicAppUrl";
        }
        internal struct ContentDistribution_Action
        {
            internal const string ContentGroupDistributionLogicAppUrl = "ContentGroupDistributionLogicAppURL";
            internal const string ContentGroupReDistributionLogicAppURL = "ContentGroupReDistributionLogicAppURL";
            //internal const string AllocationShipmentRedistributeLogicAppUrl = "AllocationShipmentRedistributeLogicAppUrl";
        }

        internal struct ContentGroup_PreOperation
        {
            internal const string ContentGroupDistributedErrorMessage = "ContentGroup_PreOperation.Messages.ContentGroupDistributedErrorMessage";
        }

        internal struct CustomerSampleAllocation_Action
        {
            internal const string CustomerSampleDistributionLogicAppUrl = "CustomerSampleDistributionLogicAppUrl";
            internal const string CustomerSampleDistributionWithRulesProfileLogicAppUrl = "CustomerSampleDistributionWithRulesProfileLogicAppUrl";
        }

        internal struct TimeOffRequest_PreOperation
        {
            internal const string AlreadyApprovedError = "TimeOffRequest_PreOperation.Messages.AlreadyApprovedError";
            internal const string FirstSubmitForReviewError = "TimeOffRequest_PreOperation.Messages.FirstSubmitForReviewError";
            internal const string AlreadyOpenError = "TimeOffRequest_PreOperation.Messages.AlreadyOpenError";
            internal const string DatesOverlappingError = "TimeOffRequest_PreOperation.Messages.DatesOverlappingError";
            internal const string CancelApprovedRequestError = "TimeOffRequest_PreOperation.Messages.CancelApprovedRequestError";
            internal const string ApprovingOpenRequestError = "TimeOffRequest_PreOperation.Messages.ApprovingOpenRequestError";
        }
        internal struct TimeOffRequest_PostOperation
        {
            internal const string ApprovedTimeOffRequestErrorMessage = "ApprovedTimeOffRequestErrorMessage";
        }

        internal struct SampleAllocationProfile_PreValidation
        {
            internal const string IncorrectDurationErrorMessage = "SampleAllocationIncorrectDurationErrorMessage";

            internal const string AllocationsDeleteErrorMessage = "AllocationsDeleteErrorMessage";

            internal const string IncorrectStartAndEndDateErrorMessage = "IncorrectStartAndEndDateErrorMessage";

            internal const string IncorrectProductErrorMessage = "SampleAllocationIncorrectProductErrorMessage";

        }

        internal struct SchedulingConfigurationField_PreValidation
        {
            internal const string DuplicateSequnceOrderMessage = "SchedulingConfigurationFieldDuplicateSequenceOrderMessage";
            internal const string DuplicateFieldMessage = "SchedulingConfigurationFieldDuplicateFieldMessage";
        }

        internal struct CoachingTemplateMeasure_PreValidation
        {
            internal const string MandatoryFieldsMissingMessage = "CoachingTemplateMeasureMandatoryFieldsMissingMessage";
            internal const string CategorrMeasureMismatchMessage = "CoachingTemplateMeasureCategorrMeasureMismatchMessage";
        }

        internal struct CoachingTemplate_PostOperation
        {
            internal const string PositionGroupAlreadyMappedMessage = "CoachingTemplate_PostOperation.Messages.PositionGroupAlreadyMapped";
        }

        internal struct PositionGroup_PostOperation
        {
            internal const string CoachingTemplateAlreadyMappedMessage = "PositionGroup_PostOperation.Messages.CoachingTemplateAlreadyMapped";
        }

        internal struct UserShipmentAllocation_PostOperation
        {
            internal const string AcknowledegedShipmentDeleteMessage = "UserShipmentAllocation_PostOperation.Messages.AcknowledegedShipmentDeleteMessage";
        }

        internal struct CustomerAllocation_PreValidation
        {
            internal const string CantDeleteDeliveredAllocationsMessage = "CustomerAllocation_PreValidation.Messages.CantDeleteDeliveredAllocationsMessage";
        }

        internal struct EmailSend_PreOperation
        {
            internal const string Fields = "Email.EmailSend_PreOperation";
            internal const string StopSendingEmailOnTimeOffRequestStatusChange = "Email.StopSendingEmailOnTimeOffRequestStatusChange";
        }

        internal struct EmailSend_PreOperation_EntitiesToMapForSubject
        {
            internal const string Fields = "Email.EmailSend_PreOperation.EntitiesToMapForSubject";
        }
        internal struct Email_OptOutURL
        {
            internal const string Fields = "Email.OptOutURL";
        }
        internal struct Email_EmailTokenReplaceActionLogic_InvalidTokens
        {
            internal const string Fields = "Email.EmailTokenReplaceActionLogic.InvalidTokens";
        }
        internal struct Email_EmailTokenReplaceActionLogic_MutlipleContactsForTokenReplacement
        {
            internal const string Fields = "Email.EmailTokenReplaceActionLogic.MutlipleContactsForTokenReplacement";
        }
        internal struct ErrorCodes
        {
            internal const string UserShipmentReAcknowledegedShipment = "IoExceptions.IO-1";
        }
        internal struct Order_PreOperation
        {
            internal const string AlreadyApproved = "Order_PreOperation.Messages.AlreadyApprovedError";
            internal const string AlreadyCancelled = "Order_PreOperation.Messages.AlreadyCancelledError";
            internal const string AlreadyNew = "Order_PreOperation.Messages.AlreadyNewError";
            internal const string AlreadyFulfilled = "Order_PreOperation.Messages.AlreadyFulfilledError";
            internal const string ApproveNotInForReview = "Order_PreOperation.Messages.ApproveNotInForReview";
            internal const string InactivePriceList = "Order_PreOperation.Messages.InactivePriceList";
            internal const string InactiveCustomer = "Order_PreOperation.Messages.InactiveCustomer";
            internal const string NoCustomer = "Order_PreOperation.Messages.NoCustomer";
            internal const string NoProduct = "Order_PreOperation.Messages.NoProduct";
        }
        internal struct AdminAlerts_PostOperation
        {
            internal const string StatusPartialFailure = "AdminAlerts_PostOperation.Messages.StatusPartialFailureError";
        }

        internal struct AutomatedCaseCreation
        {
            internal const string EscalatedExistingCaseEmailTemaplated = "AutomatedCaseCreation.EscalatedExistingCaseEmailTemaplated";
            internal const string NewCaseCreationAutoResponseTemaplated = "AutomatedCaseCreation.NewCaseCreationAutoResponseTemaplated";
        }

        internal struct IoGeneeConfiguration
        {
            internal const string BaseApiURL = "IoGeneeConfiguration.BaseAPI.URL";
            internal const string PostApprovalActivityURL = "IoGeneeConfiguration.PostApprovalActivity.URL";
            internal const string PostAdminAlertsURL = "IoGeneeConfiguration.PostAdminAlerts.URL";
            internal const string Environment = "IoGeneeConfiguration.Environment.Value";
            internal const string Administrator = "IoGeneeConfiguration.TeamAdministrator.Id";
            internal const string BusinessUnit = "IoGeneeConfiguration.TeamBusinessUnit.Id";
        }

        internal struct ActivityType_PostOperation
        {
            internal struct Messages
            {
                internal const string DuplicateActivityAndDefaultFlag = "ActivityType_PostOperation.Messages.DuplicateActivityAndDefaultFlag";
                internal const string DuplicateActivityAndMandatoryFlag = "ActivityType_PostOperation.Messages.DuplicateActivityAndMandatoryFlag";
            }
        }

        internal struct ActivitySubType_PostOperation
        {
            internal struct Messages
            {
                internal const string DuplicateActivityAndDefaultFlag = "ActivitySubType_PostOperation.Messages.DuplicateActivityAndDefaultFlag";
                internal const string DuplicateActivityAndProductMandatoryFlag = "ActivitySubType_PostOperation.Messages.DuplicateActivityAndProductMandatoryFlag";
            }
        }
    }
}
