﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Veeva
{
    public class iOConfigManager
    {

        private IOrganizationService orgService;
        private ITracingService tracService;

        public int DefaultConfigLanguage { get; }

        ////Hide the default constructor
        private iOConfigManager() { }


        //Define the parameterized constructor
        public iOConfigManager(IOrganizationService organizationService, ITracingService tracingService, int defaultConfigLanguage = 1033)
        {
            if (organizationService == default(IOrganizationService))
                throw new Exception("Organization Service cannot be null.");

            if (tracingService == default(ITracingService))
                throw new Exception("TracingService Service cannot be null.");

            orgService = organizationService;
            tracService = tracingService;
            DefaultConfigLanguage = defaultConfigLanguage;
        }

        /// <summary>
        /// Gets the configuration value for the given configuraration item
        /// </summary>
        /// <param name="configName"></param>
        /// <returns></returns>
        public string GetConfiguration(string configName, int? LCID = null)
        {
            try
            {
                #region  Get the configuration record

                QueryExpression qe = new QueryExpression(EntityAttrs.iOConfiguration.EntityName);

                qe.Criteria.AddCondition(new ConditionExpression(EntityAttrs.iOConfiguration.ConfigName, ConditionOperator.Equal, configName));

                if (LCID != null)
                    qe.Criteria.AddCondition(new ConditionExpression(EntityAttrs.iOConfiguration.Language, ConditionOperator.Equal, LCID));

                qe.ColumnSet = new ColumnSet(EntityAttrs.iOConfiguration.ConfigValue);

                Entity configRecord = orgService.RetrieveMultiple(qe).Entities.FirstOrDefault();


                //If the specified configuration item isn't found for the specified language, try to find the item for the default language
                if ((LCID != null) && (configRecord == default(Entity)))
                {
                    //Try to get the configuration of the default language
                    qe = new QueryExpression(EntityAttrs.iOConfiguration.EntityName);

                    qe.Criteria.AddCondition(new ConditionExpression(EntityAttrs.iOConfiguration.ConfigName, ConditionOperator.Equal, configName));
                    qe.Criteria.AddCondition(new ConditionExpression(EntityAttrs.iOConfiguration.Language, ConditionOperator.Equal, DefaultConfigLanguage));

                    qe.ColumnSet = new ColumnSet(EntityAttrs.iOConfiguration.ConfigValue);

                    configRecord = orgService.RetrieveMultiple(qe).Entities.FirstOrDefault();
                }

                #endregion

                string configValue = default(string);

                if (configRecord == default(Entity))
                {
                    throw new Exception("Couldn't find the configuration item: " + configName);
                }
                else
                {
                    //Get the configuration value
                    configValue = Utilities.Inst.GetAttributeValue<string>(configRecord, EntityAttrs.iOConfiguration.ConfigValue);
                }

                //Return the configuration value
                return configValue;
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
