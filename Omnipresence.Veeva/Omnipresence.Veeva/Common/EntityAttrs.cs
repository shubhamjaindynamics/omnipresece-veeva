﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Veeva
{
    public class EntityAttrs
    {
        public struct MessageTemplateAttachment
        {
            public const string EntityName = "indskr_messagetemplateattachment";
            public const string Name = "indskr_name";
            public const string Document = "indskr_document";
            public const string Resource = "indskr_resource";
            public const string Template = "indskr_messagetemplate";
        }

        public struct MobilAppForm
        {
            public const string EntityName = "indskr_mobileappform";
            public const string Validate = "indskr_validate";
            public const string Metadata = "indskr_metadata";
            public const string FormId = "indskr_formid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
        }

        public struct MobilAppRole
        {
            public const string EntityName = "indskr_mobileapprole";
            public const string MobileAppForm = "indskr_mobileappform";
        }

        public struct ProjectCallReason
        {
            public const string EntityName = "indskr_projectcallreason";
            public const string Project = "indskr_projectid";
            public const string CallReason = "indskr_callreasonid";
        }
        public struct FeatureActions
        {
            public const string EntityName = "indskr_featureaction";
            public const string FeatureAction = "indskr_featureaction";
            public const string FeatureActionId = "indskr_featureactionid";
            public const string Name = "indskr_name";
        }

        public struct SecurityRoles
        {
            public struct Administrator
            {
                public const string name = "iO Administrator";
                public static readonly Guid Id = new Guid("3FCE40EC-5662-E711-8119-480FCFF4F671");
            }
            public struct Manager
            {
                public const string name = "iO Sales Manager";
                public static readonly Guid Id = new Guid("401EE461-5A62-E711-8119-480FCFF4F671");
            }
            public struct User
            {
                public const string name = "iO Sales User";
                public static readonly Guid Id = new Guid("2409B65C-409A-E811-8155-480FCFF4F6A1");
            }
        }

        public struct OmnipresenceCaseProcess
        {
            public const string EntityName = "indskr_omnipresencecaseprocess";
            public const string ActiveStage = "activestageid";
            public const string Incident = "bpf_incidentid";
        }

        public struct OpportunitySalesProcess
        {
            public const string EntityName = "opportunitysalesprocess";
            public const string ActiveStage = "activestageid";
            public const string Opportunity = "opportunityid";
        }



        internal struct Task
        {
            public const string EntityName = "task";
            public const string Owner = "ownerid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string Subject = "subject";
            public const string Account = "indskr_account";
            public const string AccountPlan = "indskr_accountplan";
            public const string RegardingObjectId = "regardingobjectid";
            public const string ScientificPlan = "indskr_scientificplan";
            public const string Type = "indskr_plantype";
            public const string Opportunity = "indskr_opportunityid";

            internal struct TaskTypes
            {
                public const int AccountPlan = 548910000;
                public const int Scientific = 548910001;
                public const int Opportunity = 548910002;
            }
        }

        internal struct TaskAssignment
        {
            public const string EntityName = "indskr_taskassignment";
            public const string Task = "indskr_task";
            public const string User = "indskr_user";
            public const string ExternalId = "indskr_externalid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";

            public struct StatusReasons
            {
                public const int Open = 1;
                public const int Inactive = 2;
            }
        }

        public struct Opportunity
        {
            public const string EntityName = "opportunity";
            public const string AccountPlanObjective = "indskr_accountplanobjective";
            public const string AgreementsRelationship = "indskr_indskr_agreement_opportunity";
            public const string Currency = "transactioncurrencyid";
            public const string ActualRevenue = "actualvalue";
            public const string PriceList = "pricelevelid";
        }

        internal struct AccountPlan
        {
            public const string EntityName = "indskr_accountplan";
            public const string Account = "indskr_account";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";

            public struct StatusReasons
            {
                public const int Open = 1;
                public const int Inactive = 2;
                public const int Completed = 548910001;
            }

            internal struct Relationships
            {
                internal struct AccountPlanObjective
                {
                    public const string Name = "indskr_indskr_accountplan_indskr_accountplanobjective_AccountPlan";
                    public const string RelationshipEntityName = "indskr_indskr_accountplan_indskr_accountplanobjective_accountplan";
                }
                internal struct AccountContactAffiliation
                {
                    public const string EntityName = "indskr_accountcontactaffiliation";
                    public const string Name = "indskr_indskr_accountplan_indskr_accountcontacta";
                    public const string RelationshipEntityName = "indskr_indskr_accountplan_indskr_accountcontac";

                }
                internal struct Document
                {
                    public const string Name = "indskr_indskr_accountplan_indskr_iodocument";
                    public const string RelationshipEntityName = "indskr_indskr_accountplan_indskr_iodocument";
                    public const string AccountPlanId = "indskr_accountplanid";
                    public const string DocumentId = "indskr_iodocumentid";
                }
                internal struct Presentation
                {
                    public const string Name = "indskr_indskr_accountplan_indskr_iopresentation";
                    public const string RelationshipEntityName = "indskr_indskr_accountplan_indskr_iopresentatio";
                }
                internal struct Resource
                {
                    public const string Name = "indskr_indskr_accountplan_indskr_ioresource";
                    public const string RelationshipEntityName = "indskr_indskr_accountplan_indskr_ioresource";
                }
                internal struct Product
                {
                    public const string Name = "indskr_indskr_accountplan_product";
                    public const string RelationshipEntityName = "indskr_indskr_accountplan_product";
                }
                internal struct Opportunity
                {
                    public const string Name = "indskr_indskr_accountplan_opportunity_AccountPlan";
                }
            }
        }

        public struct AccountPlanObjective
        {
            public const string EntityName = "indskr_accountplanobjective";
            public const string Type = "indskr_type";
            public const string TargetAmount = "indskr_targetamount";
            public const string TargetPercent = "indskr_targetpercent";
            public const string AccountPlan = "indskr_accountplan";
            public const string CompletedPercent = "indskr_completedpercent";
            public const string CompletedValue = "indskr_completedvalue";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string Owner = "ownerid";
            public struct Types
            {
                public const int Quantitative = 548910000;
                public const int Qualitative = 548910001;
            }
            public struct StatusReasons
            {
                public const int Open = 1;
                public const int Inactive = 2;
                public const int Completed = 548910001;
            }

            public struct Relationships
            {
                public struct CriticalSuccessFactor
                {
                    public const string Name = "indskr_indskr_criticalsuccessfactors_indskr_acco";
                    public const string RelationshipEntityName = "indskr_indskr_criticalsuccessfactors_indskr_ac";
                }
            }
        }

        public struct CriticalSuccessFactor
        {
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public struct RelationShips
            {
                public struct AccountPlanObjective
                {
                    public const string Name = "indskr_indskr_criticalsuccessfactors_indskr_acco";
                    public const string RelationshipEntityName = "indskr_indskr_criticalsuccessfactors_indskr_ac";
                }
                public struct AccountPlanProgressReport
                {
                    public const string Name = "indskr_indskr_accountplanprogressreport_indskr_c";
                    public const string RelationshipEntityName = "indskr_indskr_accountplanprogressreport_indskr";
                }
            }

        }

        public struct AccountPlanProgressReport
        {
            public const string EntityName = "indskr_accountplanprogressreport";
            public const string ID = "indskr_accountplanprogressreportid";
            public const string Name = "indskr_name";
            public const string CompletedPercent = "indskr_completedpercent";
            public const string CompletedValue = "indskr_completedval";
            public const string Objective = "indskr_objective";
            public const string ProgressNotes = "indskr_progressnotes";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public struct StatusReasons
            {
                public const int Open = 1;
                public const int Inactive = 2;
                public const int ForReview = 548910000;
                public const int Approved = 548910002;
            }
        }

        internal struct CallPlanActivity
        {
            public const string EntityName = "indskr_meetingcallplan";
            public const string ID = "indskr_meetingcallplanid";
            public const string MeetingActivity = "indskr_meetingactivity";
            public const string EmailActivity = "indskr_emailactivity";
            public const string PhoneCallActivity = "indskr_phonecallactivity";
            public const string CustomerCallPlan = "indskr_customercallplan";
            public const string CreatedOn = "createdon";
        }

        internal class Account : Address1Fields
        {
            public const string EntityName = "account";
            public const string ID = "accountid";
            public const string AccountName = "name";
            public const string EmailAddress1 = "emailaddress1";
            public const string Type = "indskr_accounttype";
            public const string HospitalProfile = "indskr_hospitalprofileid";
            public const string HospitalFacilities = "indskr_hospitalfacilitiesid";
            public const string HospitalSystems = "indskr_hospitalsystemsid";
            public const string HospitalDeliveries = "indskr_hospitaldeliveriesid";
            public const string HospitalQuality = "indskr_hospitalqualityid";
            public const string RecordState = "omniveev_recordstate";
            public const string AccountSourceType = "indskr_accountsourcetype";
            public const string MDM = "indskr_mdm";
            public const string OwningBusinessUnit = "owningbusinessunit";
        }

        internal struct AccountAccountAffiliation
        {
            public const string EntityName = "indskr_accountaccountaffiliation";
            public const string AffiliatedFrom = "indskr_affiliatedfromaccountid";
            public const string AffiliatedTo = "indskr_affiliatedtoaccountid";
        }

        internal struct CallReason
        {
            public const string EntityName = "indskr_callreason";
            public const string CallReasonName = "indskr_callreason";
            public const string CallReasonDescription = "indskr_callreasondescription";

        }

        internal struct Project
        {
            public const string EntityName = "indskr_project";
            public const string ProjectName = "indskr_projectname";
            public const string ProjectStartDate = "indskr_projectstartdate";
            public const string ProjectEndDate = "indskr_projectenddate";
        }

        internal struct Disposition
        {
            public const string EntityName = "indskr_disposition";
            public const string DispositionName = "indskr_name";
        }
        internal struct BrandAffiliations
        {
            public const string EntityName = "indskr_accountbrandaffiliation";
            public const string AffiliatedFrom = "indskr_affiliatedfromid";
            public const string AffiliatedTo = "indskr_affiliatedtoid";
        }

        internal struct CustomerLicense
        {
            public const string EntityName = "indskr_customerlicense";
            public const string Country = "indskr_country";
            public const string Customer = "indskr_customer";
            public const string State = "indskr_state";
            public const string Type = "indskr_type";
            public const string CustomerAddress = "indskr_customeraddress";
            public const string ValidFrom = "indskr_validfrom";
            public const string ValidUntil = "indskr_validuntil";

        }

        internal struct ActivityParty
        {
            public const string EntityName = "activityparty";
            public const string ActivityId = "activityid";
            public const string ParticipationTypeMask = "participationtypemask";
            public const string PartyId = "partyid";
            public const string ActivityPartyId = "activitypartyid";
        }

        internal struct ActivityAccount
        {
            public const string EntityName = "indskr_activityaccount";
            public const string AccountId = "indskr_accountid";
            public const string AppointmentId = "indskr_appointmentid";
            public const string AffiliatedFrom = "indskr_affiliatedfromaccountid";
            public const string AffiliatedTo = "indskr_affiliatedtoaccountid";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivityContact
        {
            public const string EntityName = "indskr_activitycontact";
            public const string ContactId = "indskr_contactid";
            public const string AppointmentId = "indskr_appointmentid";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActiveConsent
        {
            public const string EntityName = "indskr_consent";
            public const string ID = "indskr_consentid";
            public const string Name = "indskr_name";
            public const string Campaign = "indskr_campaign";
            public const string Product = "indskr_product";
            public const string ConsentActivity = "indskr_consentactivity";
            public const string EmailAddress = "indskr_emailaddress";
            public const string SalesRep = "indskr_salesrep";
            public const string ConsentType = "indskr_consenttype";
            public const string ConsentTerms = "indskr_consentterms";
            public const string Contact = "indskr_contact";
            public const string AddressName = "indskr_addressname";
            public const string Phone = "indskr_phone";
            public const string Type = "indskr_type";
            public const string Status = "statecode";

            public struct StateCode
            {
                public const int Active = 0;
                public const int Inactive = 1;
            }

            public struct StatusCode
            {
                public const int Active = 1;
                public const int Inactive = 2;
            }
        }

        public struct Appointment
        {
            public const string EntityName = "appointment";
            public const string ActivityId = "activityid";
            public const string ScheduledStart = "scheduledstart";
            public const string ScheduledEnd = "scheduledend";
            public const string RequiredAttendees = "requiredattendees";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string Type = "indskr_type";
            public const string MeetingType = "indskr_meetingtype";
            public const string SubType = "indskr_subtype";
            public const string DateCompleted = "omnip_datecompleted";
            public const string CreatdOn = "createdon";
            public const string Position = "indskr_positionid";
            public const string Subject = "subject";
            public const string Location = "location";
            public const string MeetingUrl = "indskr_meetingurl";
            public const string AccessToken = "indskr_accesstoken";
            public const string ScreensharingUUID = "indskr_screensharinguuid";
            public const string ExternalId = "indskr_externalid";
            public const string ExternalSource = "indskr_externalsource";
            public const string ActualStart = "actualstart";
            public const string ActualEnd = "actualend";
            public const string Reason = "indskr_totreason";
            public const string AllDayEvent = "isalldayevent";
            public const string DNAscheduler = "indskr_dnascheduler";
            public const string Notes = "indskr_notes";
            public const string JointMeeting = "indskr_jointmeeting";
            public const string OfflineMeeting = "indskr_isofflinemeeting";
            public const string Owner = "ownerid";
            public const string RegardingObject = "regardingobjectid";
            public const string Delegate = "indskr_delegate";
            public const string AllocationOrder = "indskr_samplerequest";
            public const string IsRemoteDetailing = "indskr_isremotedetailing";
            public const string ActivitySubType = "indskr_activitysubtype";

            public struct StatusReasons
            {
                public const int Meeting = 100000000;
                public const int TimeOffTerritory = 100000001;
                public const int Completed = 3;
            }
        }

        internal struct Campaign
        {
            public const string EntityName = "campaign";
            public const string ID = "campaignid";
            public const string Product = "indskr_productid";
        }

        internal struct CommonAttributes
        {
            public const string StatusCode = "statuscode";
            public const string Owner = "ownerid";
        }

        internal struct Currency
        {
            public const string EntityName = "transactioncurrency";
            public const string CurrencyCode = "isocurrencycode";
        }

        internal struct ConsentActivity
        {
            public const string EntityName = "indskr_consentactivity";
            public const string ID = "indskr_consentactivityid";
            public const string Name = "indskr_name";
            public const string Campaign = "indskr_campaign";
            public const string Product = "indskr_product";
            public const string EmailAddress = "indskr_emailaddress";
            public const string Phone = "indskr_phone";
            public const string AddressName = "indskr_addressname";
            public const string SalesRep = "indskr_salesrep";
            public const string Position = "indskr_position";
            public const string Terms = "indskr_terms";
            public const string Type = "indskr_type";
            public const string Contact = "indskr_contact";
            public const string ConsentType = "indskr_consenttype";
            public const string ConsentTerms = "indskr_consentterms";
        }

        internal struct ConsentTerms
        {
            public const string EntityName = "indskr_consentterms";
            public const string ID = "indskr_consenttermsid";
            public const string Body = "indskr_body";
            public const string FromDate = "indskr_fromdate";
            public const string UntilDate = "indskr_untildate";
            public const string Name = "indskr_name";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string ConsentTermsProductRelationship = "omnip_consentterms_product";
            public const string ConsentTermsConsentChanelRelationship = "indskr_consentterms_indskr_consenttype";
        }

        internal enum ChannelType : int
        {
            Fax = 548910008,
            AllocationOrder = 548910005,
            CustomerInquiry = 548910007,
            Email = 548910001,
            Facebook = 548910002,
            SalesOrder = 548910006,
            SMS = 548910003,
            Visit = 548910000,
            Whatsapp = 548910004,
        }

        internal struct ConsentType
        {
            public const string EntityName = "indskr_consenttype";
            public const string ID = "indskr_consenttypeid";
            public const string EntityPluralName = "indskr_consenttypes";
            public const string ChannelType = "indskr_channeltype";
            public const string MessageType = "indskr_messagetype";
            public const string RequiredChannelAttributes = "indskr_requiredchannelattributes";
            public const string Name = "indskr_name";
            public const string ExternalChannel = "indskr_externalchannel";

            internal struct RequiredChannelAttributesOptions
            {
                public const int Email = 548910000;
                public const int Phone = 548910001;
                public const int Address = 548910002;
            }


        }

        internal struct ChannelProvider
        {
            public const string EntityName = "indskr_channelprovider";
            public const string ID = "indskr_channelproviderid";

            public const string Endpoint = "indskr_endpoint";
            public const string Method = "indskr_method";
            public const string Parameters = "indskr_parameters";
            public const string Headers = "indskr_headers";
            public const string Payload = "indskr_payload";
            public const string StatusCode = "statuscode";
            public const string StateCode = "statecode";
        }

        internal struct ChannelProviderLink
        {
            public const string EntityName = "indskr_channelproviderlink";
            public const string ChannelProvider = "indskr_channelprovider";
            public const string Channel = "indskr_channel";
            public const string BusinessUnit = "indskr_businessunit";
            public const string StatusCode = "statuscode";
            public const string StateCode = "statecode";
        }

        internal class Contact : Address1Fields
        {
            public const string EntityName = "contact";
            public const string ID = "contactid";
            public const string FullName = "fullname";
            public const string EmailAddress1 = "emailaddress1";
            public const string FirstName = "firstname";
            public const string LastName = "lastname";
            public const string Title = "indskr_title";
            public const string Mobile = "mobilephone";
            public const string Telephone = "telephone1";
            public const string Fax = "fax";
            public const string Specialty = "indskr_lu_specialtyid";
            public const string Language = "indskr_lu_languageid";
            public const string Kol = "indskr_kol";
            public const string Speaker = "indskr_speaker";
            public const string StatusCode = "statuscode";
            public const string OwnerId = "ownerid";
            public const string Guest = "indskr_isguest";
            public const string PrimarySpecialty = "indskr_lu_specialtyid";
            public const string PrimaryAccount = "indskr_primaryaccount";
            public const string Investigator = "ind_investigator";
            public const string GrantRecipient = "ind_grantrecipient";
            public const string RecordState = "omniveev_recordstate";
            public const string ContactSourceType = "indskr_contactsourcetype";
            public const string MDM = "indskr_mdm";
            public const string OwningBusinessUnit = "owningbusinessunit";

        }

        internal class Address1Fields
        {
            public const string Address1_Name = "address1_name";
            public const string Address1_PostOfficeBox = "address1_postofficebox";
            public const string Address1_Line1 = "address1_line1";
            public const string Address1_Line2 = "address1_line2";
            public const string Address1_Line3 = "address1_line3";
            public const string Address1_City = "address1_city";
            public const string Address1_PostalCode = "address1_postalcode";
            public const string Address1_StateProvince = "address1_stateorprovince";
            public const string Address1_Country = "address1_country";
            public const string Address1_Composite = "address1_composite";
            public const string Address1_PrimaryContactName = "address1_primarycontactname";
            public const string Address1_Telephone1 = "address1_telephone1";
            public const string Address1_Telephone2 = "address1_telephone2";
            public const string Address1_Fax = "address1_fax";
            public const string Address1_Latitude = "address1_latitude";
            public const string Address1_Longitude = "address1_longitude";
        }


        internal struct ContentToken
        {
            public const string EntityName = "indskr_contenttoken";
            public const string ID = "indskr_contenttokenid";
            public const string Name = "indskr_name";
            public const string Type = "indskr_type";
            public const string WeChatTemplatedMessageTemplate = "indskr_wechattemplatedmessagetemplate";
            public const string TokenValuesCount = "indskr_tokenvaluescount";
            public const string DefaultReplacement = "indskr_defaultreplacement";
        }

        internal struct AddressOOB
        {
            public const string EntityName = "customeraddress";
            public const string ID = "customeraddressid";
            public const string IsPrimary = "indskr_primary";
            public const string ParentId = "parentid";
            public const string Relationship = "Contact_CustomerAddress";
            public const string ContactEntity = "contact";
            public const string ContactId = "contactid";
            public const string Line1 = "line1";
            public const string City = "city";
            public const string PostalCode = "postalcode";
            public const string StateOrProvince = "stateorprovince";
            public const string Country = "country";
            public const string BestTimeToCall = "indskr_besttimetocall";
            public const string CountryCodeAlpha2 = "indskr_countrycodealpha2";
            public const string CountryCodeAlpha3 = "indskr_countrycodealpha3";
            public const string CountryNumericCode = "indskr_countrynumericcode";
            public const string CountryURLLink = "indskr_countryurllink";
            public const string StateCategory = "indskr_statecategory";
            public const string StateCode = "indskr_statecode";
            public const string Customer = "indskr_customer";
        }

        internal struct LUCountry
        {
            public const string EntityName = "indskr_lu_country";
            public const string ID = "indskr_lu_countryid";
            public const string CountryName = "indskr_countryname";
            public const string CountryCodeAlpha2 = "indskr_countrycodealpha2";
            public const string CountryCodeAlpha3 = "indskr_countrycodealpha3";
            public const string NumericCode = "indskr_numericcode";
            public const string URLLink = "indskr_urllink";
            public const string AlternateNames = "indskr_alternatenames";
            public const string SearchAltName = "indskr_searchaltnames";
        }

        internal struct LUState
        {
            public const string EntityName = "indskr_lu_state";
            public const string CountryId = "indskr_lu_countryid";
            public const string State = "indskr_state";
            public const string StateCategory = "indskr_statecategory";
            public const string StateCode = "indskr_statecode";
            public const string CountryCodeAlpha2 = "indskr_countrycodealpha2";
            public const string AlternateNames = "indskr_alternatenames";
            public const string SearchAltName = "indskr_searchaltnames";
        }

        internal struct CustomerCallPlan
        {
            public const string Name = "indskr_name";
            public const string EntityName = "indskr_customercallplan";
            public const string ID = "indskr_customercallplanid";
            public const string UserTimeZone = "usersettings5.timezonecode";
            public const string UserId = "usersettings5.systemuserid";
            public const string ParentPosition = "ParentPosition.name";
            public const string StatusCode = "statuscode";
            public const string Customer = "indskr_customerid";
            public const string Product = "indskr_productid";
            public const string CustomerSegment = "indskr_customersegment";
            public const string State = "indskr_state";
            public const string CallPlan = "indskr_cycleplanid";
            public const string IsRepCallPlan = "indskr_isrepcallplan";
            public const string ActualCalls = "indskr_actualcalls";
            public const string ActualEmails = "indskr_actualemails";
            public const string PeriodStartDate = "indskr_startdate";
            public const string PeriodEndDate = "indskr_enddate";
            public const string Position = "indskr_positionid";
            public const string Recalculate = "indskr_recalculate";
            public const string Owner = "ownerid";
            public const string CreatedBy = "createdby";

            //internal struct Relationships
            //{
            //    public const string 
            //}
        }

        internal struct CustomerPosition
        {
            public const string EntityName = "indskr_customerposition";
            public const string ID = "indskr_customerpositionid";
            public const string Customer = "indskr_customerid";
            public const string Position = "indskr_positionid";
            public const string StatusCode = "statuscode";
            public const string PositionAssociation = "indskr_positionassociation";
            internal struct StatusReasons
            {
                public const int Verified = 1;
                public const int Unverified = 548910000;
            }
        }

        internal struct CustomerSpecialty
        {
            public const string EntityName = "indskr_customerspecialty";
            public const string Name = "indskr_name";
            public const string Customer = "indskr_customerid";
            public const string Specialty = "indskr_specialtyid";
            public const string IsPrimary = "indskr_isprimary";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfieldcodeoperation";
            public const string ID = "indskr_customerspecialtyid";

        }


        internal struct Email
        {
            public const string EntityName = "email";
            public const string ID = "emailid";
            public const string EmailOverride = "indskr_emailoverride";
            public const string AddressUsed = "addressused";
            public const string ValidateConsent = "indskr_validateconsent";
            public const string RegardingObjectId = "regardingobjectid";
            public const string To = "to";
            public const string CC = "cc";
            public const string BCC = "bcc";
            public const string Description = "description";
            public const string Sender = "sender";
            public const string PositionId = "indskr_positionid";
            public const string Subject = "subject";
            public const string SendIndividually = "indskr_sendindividually";
            public const string Template = "indskr_templateid";
            public const string Regarding = "regardingobjectid";
            public const string Duration = "actualdurationminutes";
            public const string ActualStart = "actualstart";
            public const string ScheduledStart = "scheduledstart";
            public const string ScheduledEnd = "scheduledend";
            public const string From = "from";
            public const string DelayedEmailSendTime = "delayedemailsendtime";
            public const string CreatedOn = "createdon";
            public const string RecordCreatedOn = "overriddencreatedon";
            public const string ParentEmail = "indskr_parentemail";
            public const string DateTimeCreatedOnApp = "indskr_datetimecreatedonapp";
            public const string Title = "indskr_title";
            public const string Meeting = "indskr_appointmentid";
            public const string Product = "indskr_productid";
            public const string ContentURL = "indskr_contenturl";
            public const string OwnerId = "ownerid";
            public const string UserCreated = "indskr_usercreated";
            public const string DateSent = "senton";
            public const string StatusReason = "statuscode";
            public const string StateCode = "statecode";
            public const string ActivityId = "activityid";
            public const string TherapeuticArea = "indskr_therapeuticarea";
            public const string InReplyTo = "inreplyto";
            public const string SafeDescription = "safedescription";
            public const string ConversationIndex = "conversationindex";
            public const string MessageId = "messageid";
            public const string DirectionCode = "directioncode"; // 0 = Incoming, 1 = Outgoing
            public const string ExternalId = "indskr_externalid";
            public const string Channel = "indskr_channel";
            public const string ChannelProvider = "indskr_channelprovider";
            public const string PhoneCall = "indskr_phonecall";
            public const string eFaxId = "indskr_efaxid";
            public const string FaxNumber = "indskr_faxnumber";
            public const string FaxStatus = "indskr_faxstatus";
            public const string SendAsFax = "indskr_sendasfax";
            public const string CheckFaxStatus = "indskr_checkfaxstatus";
            public const string EngagementRequestActivity = "indskr_expertrequest";
            public const string EngagementRequest = "indskr_engagementrequest";
            public const string ExpertProfile = "indskr_expertprofile";
            public const string isCoVisitor = "indskr_iscovisitor";
            public const string UnresolvedTo = "indskr_unresolvedto";
            public const string UnresolvedCC = "indskr_unresolvedcc";
            public const string UnresolvedBCC = "indskr_unresolvedbcc";

            internal struct StatusReasons
            {
                public const int Sent = 3;
                public const int Sending = 7;
                public const int Failed = 8;
            }

            internal struct FaxStatuses
            {
                public const int Draft = 548910000;
                public const int Processing = 548910001;
                public const int Sent = 548910002;
                public const int Failed = 548910003;
                public const int OutOfCredit = 548910004;
            }
        }

        internal struct Template
        {
            public const string EntityName = "template";
            public const string Title = "title";
            public const string TemplateTypeCode = "templatetypecode";
            public const string IsPersonal = "ispersonal";
            public const string LanguageCode = "languagecode";
            public const string TemplateId = "templateid";
        }

        internal struct TemplateTypeCode
        {
            public const int Case = 112;
        }

        internal struct PhoneNumber
        {
            public const string EntityName = "indskr_phonenumber";
            public const string ID = "indskr_phonenumberid";
            public const string PhoneNumberAttribute = "indskr_phonenumber";
            public const string Customer = "indskr_contact";
        }

        internal struct EmailAddress
        {
            public const string EntityName = "indskr_email_address";
            public const string ID = "indskr_email_addressid";
            public const string EmailAddressAttribute = "indskr_emailaddress";
            public const string IsPrimary = "indskr_isprimary";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfield_codeoperation";
            public const string StatusCode = "statuscode";
            public const string Status = "statecode";
            public const string Customer = "indskr_customer";
            //public const string Lead = "indskr_lead";
            public const string CustomerType = "indskr_customertype";
            public const string CreatedOn = "createdon";

        }

        internal struct EmailTemplate
        {
            public const string EntityName = "indskr_emailtemplate";
            public const string Subject = "indskr_email_subject";
            public const string Body = "indskr_body";
            public const string ContentTokenRelationship = "indskr_emailtemplate_contenttoken";
            public const string ContentTokenRelationship_SchemaName = "indskr_indskr_emailtemplate_indskr_contenttoken";
            public const string EmailTemplatePositionGroupRelationship = "indskr_indskr_emailtemplate_indskr_positiongroup";
            public const string EmailTemplateContentTokenRelationship = "indskr_indskr_emailtemplate_indskr_contenttoken";
            public const string PositionGroupEmailTemplateRelationship = "indskr_indskr_emailtemplate_indskr_contenttoken";
            public const string GroupID = "indskr_groupguid";
            public const string MajorVersion = "indskr_majorversion";
            public const string MinorVersion = "indskr_minorversion";
            public const string AlternateSubject = "indskr_email_subject_alt";
            public const string From = "indskr_from";
            public const string ReplyTo = "indskr_replyto";
            public const string ThumbnailURL = "indskr_thumbnailurl";
            public const string Type = "indskr_type";
            public const string Name = "indskr_name";
            public const string ConsentChannel = "indskr_consentchannelid";
            public const string CC = "indskr_cc";
            public const string BCC = "indskr_bcc";
            public const string ContentCreatedBy = "indskr_ckmcreatedbyid";
            public const string ContentCreatedDate = "indskr_ckmcreateddate";
            public const string Description = "indskr_description";
            public const string FBBody = "indskr_fb_body";
            public const string SMSBody = "indskr_sms_body";
            public const string Product = "omnip_product";
            public const string TherapeuticArea = "indskr_therapeuticarea";
            public const string WhatsappBody = "indskr_whatsapp_body";
            public const string VersionBy = "indskr_ckmversionbyid";
            public const string VersionDate = "indskr_ckmversiondate";
            public const string VersionID = "indskr_ckmversionid";
            public const string PublishedDate = "indskr_ckmpublisheddate";
            public const string PublishedBy = "indskr_ckmpublishedbyid";
            public const string Status = "statecode";
            public const string StatusCode = "statuscode";
            public const string MessageTemplateId = "indskr_emailtemplateid";


        }

        internal struct HospitalProfile
        {
            public const string EntityName = "indskr_hospitalprofile";
            public const string Account = "indskr_account";
            public const string HospitalName = "indskr_hospitalname";
        }

        internal struct HospitalFacilities
        {
            public const string EntityName = "indskr_hospitalfacilities";
            public const string Account = "indskr_accountid";
            public const string HospitalName = "indskr_hospitalname";
        }

        internal struct HospitalSystems
        {
            public const string EntityName = "indskr_hospitalsystems";
            public const string Account = "indskr_accountid";
            public const string HospitalName = "indskr_hospitalname";
        }

        internal struct HospitalDeliveries
        {
            public const string EntityName = "indskr_hospitaldeliveries";
            public const string Account = "indskr_accountid";
            public const string HospitalName = "indskr_hospitalname";
        }

        internal struct HospitalQuality
        {
            public const string EntityName = "indskr_hospitalquality";
            public const string Account = "indskr_accountid";
            public const string HospitalName = "indskr_hospitalname";
        }

        internal struct iOConfiguration
        {
            public const string EntityName = "indskr_ioconfiguration";
            public const string ConfigName = "indskr_configname";
            public const string ConfigValue = "indskr_configvalue";
            public const string Description = "indskr_description";
            public const string Language = "indskr_language";
        }

        internal struct MarketingList
        {
            public const string EntityName = "list";
            public const string ID = "listid";
        }

        internal struct Position
        {
            public const string EntityName = "position";
            public const string Id = "positionid";
            public const string ParentPosition = "parentpositionid";
            public const string PositionProductAssignmentRelation = "indskr_position_indskr_productassignment";
            public const string PositionProductRelation = "indskr_position_product";
            public const string PositionUserRelation = "indskr_position_systemuser";
            public const string PositionUserPositionRelation = "indskr_position_userposition_positionid";
            public const string PositionContactRelation = "indskr_position_contact";
            public const string PositionCustomerContactRelation = "indskr_position_customerposition_positionid";
            public const string PositionAccountRelation = "indskr_position_account";
            public const string Status = "statecode";
            public const string State = "statuscode";
            public const string PositionParentPositionRelation = "position_parent_position";
        }

        internal struct PositionAddress
        {
            public const string EntityName = "indskr_positionaddress";
            public const string ID = "indskr_positionaddressid";
            public const string Position = "indskr_position";
            public const string Address = "indskr_address";
            public const string IsPrimary = "indskr_isprimary";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfieldcodeoperation";
        }

        internal struct Presentation
        {
            public const string EntityName = "indskr_iopresentation";
            public const string PresentationPositionGroupRelationship = "indskr_positiongroups_iopresentation";
            public const string ParentPresentation = "indskr_parentpresentation";
            public const string GroupID = "indskr_groupguid";
            public const string MajorVersion = "indskr_majorversion";
            public const string MinorVersion = "indskr_minorversion";
            public const string Title = "indskr_title";
            public const string ProcuctId = "indskr_productid";
            public const string PollingTemplate = "indskr_pollingtemplate";
            public const string NumberOfPages = "indskr_numberofpages";
            public const string Name = "indskr_name";
            public const string LanguageId = "indskr_languageid";
            public const string Description = "indskr_description";
            public const string CKMZipURL = "indskr_ckmzipurl";
            public const string CKMVersionId = "indskr_ckmversionid";
            public const string CKMVersionDate = "indskr_ckmversiondate";
            public const string CKMVersionById = "indskr_ckmversionbyid";
            public const string CKMTitle = "indskr_ckmtitle";
            public const string CKMThumbnailZipURL = "indskr_ckmthumbnailszipurl";
            public const string CKMThumbnailURL = "indskr_ckmthumbnailurl";
            public const string CKMTag = "indskr_ckmtag";
            public const string CKMPublishedDate = "indskr_ckmpublisheddate";
            public const string CKMPublishedById = "indskr_ckmpublishedbyid";
            public const string CKMPresentationId = "indskr_ckmpresentationid";
            public const string CKMDescription = "indskr_ckmdescription";
            public const string CKMDefaultWidth = "indskr_ckmdefaultwidth";
            public const string CKMDefaultResolution = "indskr_ckmdefaultresolution";
            public const string CKMDefaultHeight = "indskr_ckmdefaultheight";
            public const string CKMCreatedDate = "indskr_ckmcreateddate";
            public const string CKMCreatedById = "indskr_ckmcreatedbyid";
            public const string BusinessMessageId = "indskr_businessmessageid";
            public const string AvailableUntil = "indskr_availableuntil";
            public const string AvailableFrom = "indskr_availablefrom";
            public const string PresentationId = "indskr_iopresentationid";
            public const string StatusCode = "statuscode";
            public const string FilesData = "indskr_filesdata";
            public struct StatusReasons
            {
                public const int Inactive = 2;
                public const int Published = 548910000;
                public const int Archived = 548910001;
            }
        }

        internal struct Page
        {
            public const string EntityName = "indskr_iopage";
            public const string Name = "indskr_name";
            public const string VirtualPageValue = "indskr_virtualpagevalue";
            public const string VirtualPageType = "indskr_virtualpagetype";
            public const string Title = "indskr_title";
            public const string IsVirtualPage = "indskr_isvirtualpage";
            public const string Description = "indskr_description";
            public const string CKMPageURL = "indskr_ckmpageurl";
            public const string CKMPageTitle = "indskr_ckmpagetitle";
            public const string CKMPageThumbnailURL = "indskr_ckmpagethumbnailurl";
            public const string CKMPageSequence = "indskr_ckmpagesequence";
            public const string CKMPageNote = "indskr_ckmpagenote";
            public const string CKMPageId = "indskr_ckmpageid";
            public const string CKMPageDescription = "indskr_ckmpagedescription";
            public const string PresentationId = "indskr_iopresentationid";
        }

        internal struct Documents
        {
            public const string EntityName = "indskr_iodocument";
            public const string DocumentPositionGroupRelationship = "indskr_positiongroups_iodocument";
            public const string ParentDocument = "indskr_parentdocument";
            public const string GroupID = "indskr_groupguid";
            public const string MajorVersion = "indskr_majorversion";
            public const string MinorVersion = "indskr_minorversion";
            public const string Title = "indskr_title";
            public const string LanguageId = "indskr_languageid";
            public const string CKMVersionId = "indskr_ckmversionid";
            public const string CKMVersionDate = "indskr_ckmversiondate";
            public const string CKMVersionById = "indskr_ckmversionbyid";
            public const string CKMTitle = "indskr_ckmtitle";
            public const string CKMThumbmnailURL = "indskr_ckmthumbnailurl";
            public const string CKMPublishedDate = "indskr_ckmpublisheddate";
            public const string CKMPublishedById = "indskr_ckmpublishedbyid";
            public const string CKMDescription = "indskr_ckmdescription";
            public const string CKMCreatedDate = "indskr_ckmcreateddate";
            public const string CKMCreatedById = "indskr_ckmcreatedbyid";
            public const string CKMDocumentURL = "indskr_ckmdocumenturl";
            public const string CKMDocumentType = "indskr_ckmdocumenttype";
            public const string CKMDocumentId = "indskr_ckmdocumentid";
            public const string AvailableUntil = "indskr_availableuntil";
            public const string AvailableFrom = "indskr_availablefrom";
            public const string DocumentId = "indskr_iodocumentid";
            public const string StatusCode = "statuscode";
            public const string DocumentURL = "indskr_ckmdocumenturl";
        }

        /// <summary>
        /// Diaplay Name: Process
        /// </summary>
        internal struct Process
        {
            public const string EntityName = "workflow";
            public const string Name = "name";
            public const string WorkflowID = "workflowid";
            public const string Type = "type";
            public const string OnDemand = "ondemand";

        }

        internal struct Product
        {
            public const string EntityName = "product";
            public const string Name = "name";
            public const string ID = "productid";
            public const string ConsentTerms = "indskr_consentterms";
            public const string ProductType = "indskr_producttype";
            public const string Status = "statecode";
            public const string CreatedOn = "createdon";
            public const string ParentProductId = "indskr_parentproductid";
            public const string CompetitorProduct = "indskr_competitorproduct";
            public const string CompetitorSKU = "indskr_competitorsku";
            internal struct Relationships
            {
                internal struct CompetitorProduct
                {
                    public const string Name = "indskr_product_product_competitor_products";
                    public const string RelationshipEntityName = "indskr_product_product_competitor_products";
                }
                internal struct CompetitorSKU
                {
                    public const string Name = "indskr_product_product_competitor_sku";
                    public const string RelationshipEntityName = "indskr_product_product_competitor_sku";
                }

            }


        }

        internal struct ProductBatch
        {
            public const string EntityName = "indskr_productbatch";
            public const string ID = "indskr_productbatchid";
            public const string Description = "indskr_description";
            public const string ExpirationDate = "indskr_expirationdate";
            public const string ExternalID = "indskr_externalid";
            public const string ExternalSource = "indskr_externalsource";
            public const string ManufactureDate = "indskr_manufacturedate";
            public const string ManufactureLocation = "indskr_manufacturelocation";
            public const string SKU = "indskr_sku";
            public const string TimeZoneCode = "indskr_timezonecode";
            public const string UTCConversionTimeZoneCode = "utcconversiontimezonecode";
        }


        internal struct ProductAssignment
        {
            public const string EntityName = "indskr_productassignment";
            public const string Position = "indskr_positionid";
            public const string Product = "indskr_productid";
            public const string ProductAssignmentType = "indskr_productassignmenttype";
            public const string PositionAssociation = "indskr_positionassociation";
        }

        internal struct HCPSegmentation
        {
            public const string EntityName = "indskr_productrating";
            public const string Product = "indskr_product";
            public const string Contact = "indskr_contact";
            public const string StatusCode = "statuscode";
            public const string Segmentation = "indskr_segmentation";
            public const string Intimacy = "indskr_intimacy";
            public const string ID = "indskr_productratingid";
        }

        internal struct ProcessTrigger
        {
            public const string EntityName = "indskr_processtrigger";
            public const string ProcessLogicalName = "indskr_processname";
            public const string ProcessType = "indskr_processtype";
            public const string ProcessArguments = "indskr_processarguments";
            public const string TargetEntityLogicalName = "indskr_targetentitylogicalname";
            public const string Remarks = "indskr_remarks";
            public const string DeletePermanently = "indskr_deletepermanently";
            public const string Name = "indskr_name";
            public const string OrganizationURL = "indskr_organizationurl";

            internal struct ProcessTypes
            {
                public const bool Action = true;
                public const bool Workflow = false;
            }
        }

        internal struct Resource
        {
            public const string EntityName = "indskr_ioresource";
            public const string ResourcePositionGroupRelationship = "indskr_positiongroups_ioresource";
            public const string GroupID = "indskr_groupguid";
            public const string MajorVersion = "indskr_majorversion";
            public const string MinorVersion = "indskr_minorversion";
            public const string ResourceId = "indskr_ioresourceid";
            public const string StatusCode = "statuscode";
            public const string ResourceURL = "indskr_ckmasseturl";
        }

        internal struct TrackChanges
        {
            public const string EntityName = "indskr_trackchange";
            public const string ID = "indskr_trackchangeid";
            public const string Action = "indskr_action";
            public const string entityId = "indskr_entityid";
            public const string entityName = "indskr_entityname";
            public const string Operation = "indskr_operation";
            public const string OwnerId = "indskr_ownerid";
            public const string PositionId = "indskr_positionid";
            public const string ProductType = "indskr_producttype";
            public const string UserId = "indskr_userid";
            public const string PositionGroup = "indskr_positiongroup";
            public const string Product = "indskr_product";
            public const string AdditionalInfo = "indskr_additionalinfo";
        }

        internal struct ScientificPlan
        {

            public const string EntityName = "indskr_scientificplan";
            public const string ID = "indskr_scientificplanid";
            public const string CalculationFlag = "indskr_calculatetotalsflag";
            public const string CalcuationConfiguration = "indskr_calculationformula";
            public const string Name = "indskr_name";
            public const string StartDate = "indskr_startdate";
            public const string EndDate = "indskr_enddate";
            public const string ScientificPlanMeetingsCompleted = "indskr_scientificplantotalmeetings";
            public const string ScientificPlanMessagesCompleted = "indskr_scientificplantotalmessages";
            public const string GracePeriodDate = "indskr_graceperioddate";
            public const string Products = "indskr_products";
            public const string TherapeuticAreas = "indskr_therapeuticareas";
            public const string EndDateUserLocal = "indskr_enddateuserlocal";
            internal struct CalculationConfigOptions
            {
                public const int OnlyProducts = 548910000;
                public const int OnlyTA = 548910001;
                public const int BothProductandTA = 548910002;
            }
            public const string CustomerRelationship = "indskr_indskr_scientificplan_contact";
            public const string ProductRelationship = "indskr_scientificplan_product";
            public const string TherapeuticareaRelationship = "indskr_scientificplan_therapeuticarea";
            public const string UserRelationship = "indskr_scientificplan_systemuser";
            public const string DocumentRelationship = "indskr_indskr_scientificplan_indskr_iodocument";
            public const string PresentationRelationship = "indskr_indskr_scientificplan_indskr_iopresentati";
            public const string ResourceRelationship = "indskr_indskr_scientificplan_indskr_ioresource";

            internal struct StatusReasons
            {
                public const int Draft = 1;
                public const int UnderRevision = 548910001;
                public const int Completed = 2;
                public const int Cancelled = 548910002;
                public const int Published = 548910003;
            }
        }

        internal struct ScientificPlanActivity
        {
            public const string EntityName = "indskr_scientificplanactivity";
            public const string ID = "indskr_scientificplanactivityid";
            public const string MeetingActivity = "indskr_meetingactivity";
            public const string EmailActivity = "indskr_emailactivity";
            public const string ScientificPlan = "indskr_scientificplan";
            public const string Name = "indskr_name";
            public const string CreatedOn = "createdon";
        }

        internal struct TokenReplacement
        {
            public const string EntityName = "indskr_content_token_value";
            public const string ID = "indskr_content_token_valueid";
            public const string ContentToken = "indskr_contenttoken";
            public const string Value = "indskr_value";
            public const string Name = "indskr_name";
            public const string IsDefault = "indskr_is_default";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfield_codeoperation";
            public const string StatusCode = "statuscode";
        }

        internal struct User
        {
            public const string EntityName = "systemuser";
            public const string FullName = "fullname";
            public const string Position = "positionid";
            public const string StandardName = "standardname";
            public const string TimeZoneCode = "timezonecode";
            public const string ID = "systemuserid";
            public const string BusinessUnitId = "businessunitid";
            // added additional attributes - jchoo - 2018-05-28
            public const string FirstName = "firstname";
            public const string LastName = "lastname";
            public const string Email = "internalemailaddress";
            public const string PrimaryPhone = "address1_telephone1";
            public const string Fax = "address1_fax";
            //public const string Address1_Composite = "address1_composite"; 
            public const string Manager = "parentsystemuserid";

        }

        internal struct Teams
        {
            public const string EntityName = "team";
            public const string BusinessUnit = "businessunitid";
            public const string TeamId = "teamid";
            public const string TeamType = "teamtype";
            public const string Administrator = "administratorid";
            public const string Name = "name";
        }

        internal struct ApprovalActivity
        {
            public const string EntityName = "indskr_approvalactivity";
            public const string Approver = "indskr_approver";
            public const string Status = "statuscode";


            public struct StatusReasons
            {
                public const int Draft = 1;
                public const int Pending = 548910000;
                public const int Approved = 548910001;
                public const int NotApproved = 548910002;

            }
        }

        internal struct BusinessUnitApprovalConfiguration
        {
            public const string EntityName = "indskr_businessunitapprovalconfiguration";
            public const string ID = "indskr_businessunitapprovalconfigurationid";
            public const string ApprovalConfiguration = "indskr_approvalconfigurationid";
            public const string BusinessUnit = "indskr_businessunitid";
        }

        internal struct ApprovalConfiguration
        {
            public const string EntityName = "indskr_approval_configuration";
            public const string ID = "indskr_approval_configurationid";
            public const string ApprovalItem = "indskr_approvalitem";
        }

        internal struct Approver
        {
            public const string EntityName = "indskr_approver_type";
            public const string Assigned = "ownerid";
            public const string OwningBusinessUnit = "owningbusinessunit";

        }


        internal struct UserAllocationTransfer
        {
            public const string EntityName = "indskr_userallocationtransfer";
            public const string Lot = "indskr_lot";
            public const string QuantityTransferred = "indskr_quantitytransferred";
            public const string Reason = "indskr_reason";
            public const string TransferDate = "indskr_transferdate";
            public const string TransferredBy = "indskr_transferredby";
            public const string TransferredTo = "indskr_transferredto";
            public const string TransferStatus = "indskr_transferstatus";

            internal struct TransferStatuses
            {
                public const int Transferred = 548910000;
                public const int Acknowledged = 548910001;
            }
        }
        internal struct UserPosition
        {
            public const string EntityName = "indskr_userposition";
            public const string ID = "indskr_userpositionid";
            public const string User = "indskr_userid";
            public const string Position = "indskr_positionid";
            public const string Primary = "indskr_primaryposition";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfield_codeoperation";
            public const string PositionAssociation = "indskr_positionassociation";
        }

        internal struct UserSettings
        {
            public const string EntityName = "usersettings";
            public const string TimeZoneCode = "timezonecode";
            public const string SystemUserId = "systemuserid";
            public const string UILanguageID = "uilanguageid";
            public const string LocaleId = "localeid";
        }

        internal struct View
        {
            public const string EntityName = "savedquery";
            public const string Name = "name";

        }

        internal struct WeChatFollow
        {
            public const string EntityName = "indskr_wechatfollow";
            public const string WeChatAccount = "indskr_wechataccount";
            public const string Contact = "indskr_contact";
        }

        internal struct WeChatFollowActivity
        {
            public const string EntityName = "indskr_wechatfollowactivity";
            public const string WeChatAccount = "indskr_wechataccount";
            public const string Contact = "indskr_contact";
            public const string ActivityTime = "indskr_activitytime";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivityContentShare
        {
            public const string EntityName = "indskr_activitycontentshare";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivitySharedResource
        {
            public const string EntityName = "indskr_activitysharedresource";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivityVoipCustomer
        {
            public const string EntityName = "indskr_activityvoipcustomer";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct RemoteRequest
        {
            public const string EntityName = "indskr_remoterequest";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ContactRelationship
        {
            public const string EntityName = "indskr_contactrelationship";
            public const string Contact = "indskr_contactid";
            public const string ID = "indskr_contactrelationshipid";
            public const string RelatedContact = "indskr_relatedcontactid";
            public const string Relationship = "indskr_relationship";
            public const string Comments = "indskr_comments";
        }

        internal struct AccountContactAffiliation
        {
            public const string EntityName = "indskr_accountcontactaffiliation";
            public const string Contact = "indskr_contactid";
            public const string ID = "indskr_accountcontactaffiliationid";
            public const string Account = "indskr_accountid";
            public const string Comments = "indskr_comments";
            public const string StartDate = "indskr_startdate";
            public const string EndDate = "indskr_enddate";
            public const string StatusCode = "statuscode";
            public const string DirectRelationshipFlag = "indskr_directrelationshipflag";
            public const string Role = "indskr_contactrole";
            public const string IsPrimaryAccount = "indskr_isprimaryaccount";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfieldcodeoperation";
            public const string AffiliationRole = "omnione_role";
        }

        internal struct AccountLeadAffiliation
        {
            public const string EntityName = "indskr_accountleadaffiliation";
            public const string Lead = "indskr_leadid";
            public const string ID = "indskr_accountleadaffiliationid";
            public const string Account = "indskr_accountid";
            public const string Comments = "indskr_comments";
            public const string StartDate = "indskr_startdate";
            public const string EndDate = "indskr_enddate";
            public const string StatusCode = "statuscode";
            public const string DirectRelationshipFlag = "indskr_directrelationshipflag";
            public const string Role = "indskr_leadtrole";
            public const string IsPrimaryAccount = "indskr_isprimary";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfieldcodeoperation";
        }

        internal struct CustomerReachFrequency
        {
            public const string EntityName = "indskr_customerreachfrequency";
            public const string ID = "indskr_customerreachfrequencyid";
            public const string Customer = "indskr_customer";
            public const string Product = "indskr_product";
            public const string Channel = "indskr_channel";
        }

        internal struct SegmentReachFrequency
        {
            public const string EntityName = "indskr_segmentreachfrequency";
            public const string ID = "indskr_segmentreachfrequencyid";
            public const string Segment = "indskr_segment";
            public const string Product = "indskr_product";
            public const string Channel = "indskr_channel";
        }

        internal struct ActivityPlaylist
        {
            public const string EntityName = "indskr_activityplaylist";
            public const string Appointment = "indskr_appointmentid";
            public const string Presentation = "indskr_iopresentationid";
            public const string Name = "indskr_name";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivityResource
        {
            public const string EntityName = "indskr_activityresource";
            public const string Appointment = "indskr_appointmentid";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivityAttendeeResource
        {
            public const string EntityName = "indskr_activityattendeeresource";
            public const string Appointment = "indskr_appointmentid";
            public const string PhoneCall = "indskr_phonecallid";
        }


        internal struct ActivityPresentation
        {
            public const string EntityName = "indskr_activitypresentation";
            public const string ID = "indskr_activitypresentationid";
            public const string Appointment = "indskr_appointmentid";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivityPresentationSlide
        {
            public const string EntityName = "indskr_activitypresentationslide";
            public const string ID = "indskr_activitypresentationslideid";
            public const string ActivityPresentation = "indskr_activitypresentationid";
        }

        internal struct ActivityAttendeeSlide
        {
            public const string EntityName = "indskr_activitycontactslide";
            public const string Appointment = "indskr_appointmentid";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct KeyMessage
        {
            public const string EntityName = "indskr_keymessage";
            public const string ID = "indskr_keymessageid";
            public const string Description = "indskr_description";
            public const string ExternalId = "indskr_externalid";
            public const string Language = "indskr_languageid";
        }

        internal struct ActivityProductKeyMessage
        {
            public const string EntityName = "indskr_activityproductkeymessage";
            public const string GeneeSelected = "indskr_geneeselected";
            public const string Name = "indskr_name";
            public const string KeyMessage = "indskr_keymessageid";
            public const string Product = "indskr_productid";
            public const string Appointment = "indskr_appointmentid";
            public const string ActivityProduct = "indskr_activityproductid";
            public const string ExternalId = "indskr_externalid";
            public const string ExternalSource = "indskr_externalsource";
            public const string AutomaticallySelected = "indskr_isautomaticallyselected";
        }

        internal struct ActivityProduct
        {
            public const string EntityName = "indskr_activityproduct";
            public const string ID = "indskr_activityproductid";
            public const string Product = "indskr_productid";
            public const string Appointment = "indskr_appointmentid";
            public const string GeneeSelected = "indskr_geneeselected";
            public const string Name = "indskr_name";
            public const string Priority = "indskr_sequence";
            public const string AutomaticallySelected = "indskr_isautomaticallyselected";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct ActivityTherapeuticArea
        {
            public const string EntityName = "indskr_activitytherapeuticarea";
            public const string ID = "indskr_activitytherapeuticareaid";
            public const string TherapeuticArea = "indskr_therapeuticarea";
            public const string Appointment = "indskr_appointment";
            public const string GeneeSelected = "indskr_geneeselected";
            public const string Name = "indskr_name";
            public const string Priority = "indskr_sequence";
            public const string AutomaticallySelected = "indskr_automaticallyselected";
            public const string PhoneCall = "indskr_phonecallid";
        }


        internal struct PhoneCall
        {
            public const string EntityName = "phonecall";
            public const string VanityURL = "indskr_vanityurl";
            public const string AccessToken = "indskr_accesstoken";
            public const string MeetngURL = "indskr_meetingurl";
            public const string CallTo = "to";
            public const string ID = "activityid";
            public const string DateCompleted = "indskr_datecompleted";
            public const string ActualStart = "actualstart";
            public const string ActualEnd = "actualend";
            public const string Position = "indskr_positionid";

        }

        internal struct ActivityAttendees
        {
            public const string EntityName = "indskr_activitycontact";
            public const string Contact = "indskr_contactid";
            public const string Appointment = "indskr_appointmentid";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct Organization
        {
            public const string EntityName = "organization";
            public const string Language = "languagecode";
        }

        internal struct PriceList
        {
            public const string EntityName = "pricelevel";
            public const string Currency = "transactioncurrencyid";
            public const string Status = "statecode";
        }

        internal struct Quote
        {
            public const string EntityName = "quote";
            public const string Opportunity = "opportunityid";
        }

        internal struct Queue
        {
            public const string EntityName = "queue";
            public const string EmailAddress = "emailaddress";
        }

        internal struct Order
        {
            public const string EntityName = "salesorder";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string OrderNumber = "ordernumber";
            public const string Name = "name";
            public const string Country = "indskr_country";
            public const string Currency = "transactioncurrencyid";
            public const string PriceList = "pricelevelid";
            public const string Account = "customerid";
            public const string Opportunity = "opportunityid";
            public const string TotalAmount = "totalamount";


            internal struct Statuses
            {
                public const int Active = 0;
                public const int Submitted = 1;
                public const int Canceled = 2;
                public const int Fulfilled = 3;
                public const int Invoiced = 4;
            }

            internal struct StatusReasons
            {
                public const int OnHold = 690970000;
                public const int InProgress = 3;
                public const int NoMoney = 4;
                public const int New = 1;
                public const int Pending = 2;
                public const int Complete = 100001;
                public const int Partial = 100002;
                public const int Invoiced = 100003;
                public const int ForReview = 548910000;
                public const int Approved = 548910001;
            }

            internal struct Relationships
            {
                internal struct ProductsRelationship
                {
                    internal const string RelationshipName = "order_details";
                }
            }
        }


        internal struct OrderProduct
        {
            public const string EntityName = "salesorderdetail";
            public const string Order = "salesorderid";
            public const string Product = "productid";

        }


        internal struct CountryAccountPriceList
        {
            public const string EntityName = "indskr_accountpricelist";
            public const string Account = "indskr_account";
            public const string Country = "indskr_country";
            public const string PriceList = "indskr_pricelist";
        }

        internal struct PriceListItem
        {
            public const string EntityName = "productpricelevel";
            public const string PriceList = "pricelevelid";

        }
        internal struct RecEngagementPeriod
        {
            public const string EntityName = "indskr_rec_engagement_period";
            public const string Customer = "indskr_customer";
            public const string Lead = "indskr_lead";
            public const string Channel = "indskr_channel";
            public const string Address = "indskr_address";
            public const string DayOfWeek = "indskr_day_of_week";
            public const string DayOrder = "indskr_dayorder";
            public const string TimeSlot = "indskr_time_slot";
            public const string TimeSlotOrder = "indskr_timeslotorder";
            internal struct ChannelOptions
            {
                public const int Email = 100000001;
                public const int Fax = 100000002;
                public const int Mail = 100000000;
                public const int Meeting = 100000005;
                public const int PhoneCall = 100000003;
                public const int WeChatTemplatedMessage = 100000006;
            }

        }

        internal struct City
        {
            public const string EntityName = "indskr_lu_city";
            public const string Name = "indskr_name";
            public const string State = "indskr_state";
            public const string AlternateNames = "indskr_alternatenames";
            public const string SearchAltName = "indskr_searchaltnames";
        }

        internal struct CustomerAvailability
        {
            public const string EntityName = "indskr_customeravailability_v2";
            public const string CustomerAddress = "indskr_customeraddressid";
            public const string DayOfWeek = "indskr_dayofweek";
            public const string From = "indskr_from";
            public const string FromOrder = "indskr_fromorder";
            public const string To = "indskr_to";
        }

        internal struct CustomerAddress
        {
            public const string EntityName = "indskr_indskr_customeraddress_v2";
            public const string ID = "indskr_indskr_customeraddress_v2id";
            public const string Customer = "indskr_customer";
            public const string CustomerType = "indskr_customeridtype";
            public const string Address = "indskr_address";
            public const string AddressId = "indskr_addressid";
            public const string Position = "indskr_position";
            public const string IsPrimary = "indskr_isprimary";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfield_codeoperation";

        }

        internal class Lead : Address1Fields
        {
            public const string EntityName = "lead";
            public const string EmailAddress1 = "emailaddress1";
            public const string PrimaryAccount = "indskr_primaryaccount";
        }

        internal struct LeadAvailability
        {
            public const string EntityName = "indskr_leadavailability";
            public const string LeadAddress = "indskr_leadaddressid";
            public const string DayOfWeek = "indskr_dayofweek";
            public const string From = "indskr_from";
            public const string To = "indskr_to";
        }

        internal struct LeadAddress
        {
            public const string EntityName = "indskr_leadaddress";
            public const string ID = "indskr_leadaddressid";
            public const string Lead = "indskr_leadid";
            public const string Address = "indskr_address";
            public const string IsPrimary = "indskr_isprimary";
            public const string HiddenFieldCodeOperation = "indskr_hiddenfield_codeoperation";
        }

        internal struct PostalCode
        {
            public const string EntityName = "indskr_lu_postalcode";
            public const string Id = "indskr_lu_postalcodeid";
            public const string PostalCodeField = "indskr_postalcode";
            public const string City = "indskr_city";
            public const string TimeZone = "indskr_timezone";
            public const string PositionRelationName = "indskr_indskr_lu_postalcode_position";
            public const string BrickRelationName = "indskr_brick_indskr_lu_postalcode_brick";
            public const string AddressRelationName = "indskr_indskr_lu_postalcode_indskr_address_postalcode_lu";
            public const string Position = "positionid";
            public const string Brick = "indskr_brick";


        }

        internal struct Address
        {
            public const string EntityName = "indskr_address";
            public const string Id = "indskr_addressid";
            public const string Name = "indskr_name";
            public const string PostBoxNo = "indskr_postofficebox";
            public const string Street1 = "indskr_line1";
            public const string Street2 = "indskr_line2";
            public const string Street3 = "indskr_line3";
            public const string City = "indskr_city_lu";
            public const string PostalCode = "indskr_postalcode_lu";
            public const string Brick = "indskr_brick";
            public const string State = "indskr_state_lu";
            public const string Country = "indskr_country_lu";
            public const string Composite = "indskr_composite";
            public const string CompositeCityState = "indskr_compositecitystate";
            public const string PrimaryContact = "indskr_primarycontact";
            public const string Phone1 = "indskr_telephone1";
            public const string Phone2 = "indskr_telephone2";
            public const string Fax1 = "indskr_fax";
            public const string Latitude = "indskr_latitude";
            public const string Longitude = "indskr_longitude";
            public const string TimeZone = "indskr_timezone";
            public const string ExternalID = "indskr_externalid";
            public const string InternalComposite = "indskr_compositeinternal";
        }

        internal struct BusinessUnit
        {
            public const string EntityName = "businessunit";
            public const string Language = "omnip_language";
            public const string OutboundEmailQueue = "indskr_outboundemailqueue";
            public const string ContactCenterVanityURL = "indskr_contactcentervanityurl";
            public const string Id = "businessunitid";
            public const string MapCustomersTo = "indskr_customertopositionmapping";
            public const string MarketScanUpdateDuration = "indskr_marketscanupdateduration";
            public const string eFaxURL = "indskr_efaxurl";
            public const string eFaxUsername = "indskr_efaxusername";
            public const string eFaxPassword = "indskr_efaxpassword";
            public const string category = "indskr_category";
            public const string ParentBU = "parentbusinessunitid";
            public const string DeactivateTheAccounts = "indskr_deactivatetheaccounts";
            public const string SubmitAccountForApproval = "indskr_submittheaccountforapproval";
            public const string ConvertToBusinessAccount = "indskr_converttobusinessaccount";
            public const string DeactivateTheContacts = "indskr_deactivatethecontacts";
            public const string SubmitContactForApproval = "indskr_submitthecontactforapproval";
            public const string ConvertToBusinessContact = "indskr_converttobusinesscontact";

            public enum Categories
            {
                Region = 548910000,
                Company = 548910001,
                GlobalArea = 548910002,
                BusinessArea = 548910003,
                FunctionalArea = 548910004,
                Sector = 548910005
            }
        }

        internal struct AsyncOperations
        {
            public const string EntityName = "asyncoperation";
            public const string Name = "name";
            public const string PostPoneUntil = "postponeuntil";
            public const string CreatedOn = "createdon";
        }

        internal struct Common
        {
            public const string Name = "name";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
        }

        internal struct Area
        {
            public const string EntityName = "indskr_lu_area";
            public const string Name = "indskr_name";
            public const string City = "indskr_city";

        }

        internal struct District
        {
            public const string EntityName = "indskr_lu_district";
            public const string Name = "indskr_name";
            public const string State = "indskr_stateprovince";

        }

        internal struct RoutingConfigurationRule
        {
            public const string EntityName = "indskr_routingconfigurationrule";
            public const string RoutingRule = "indskr_routingruleid";
            public const string RoutingConfiguration = "indskr_routingconfigurationid";

        }

        internal struct UserSkill
        {
            public const string EntityName = "indskr_usercustomskill";
            public const string User = "indskr_userid";
            public const string Skills = "indskr_customskillid";

        }

        internal struct Audit
        {
            public const string EntityName = "audit";
            public const string AuditID = "auditid";
            public const string ObjectTypeCode = "objecttypecode";
            public const string ObjectID = "objectid";
            public const string Operation = "operation";
            public const string Action = "action";
            public const string AttributeMask = "attributemask";
            public const string UserID = "userid";
            public const string TransactionID = "transactionid";
            public const string CreatedOn = "createdon";
        }

        internal struct TherapeuticArea
        {
            public const string EntityName = "indskr_therapeuticarea";
            public const string TherapeuticAreaId = "indskr_therapeuticareaid";
            public const string UserRelationShip = "indskr_indskr_therapeuticarea_systemuser";
            public const string CampaignRealtionship = "indskr_campaign_indskr_therapeuticarea";
            public const string PresentationRelationship = "indskr_indskr_therapeuticarea_indskr_iopresent";
            public const string Name = "indskr_name";
            public const string PresentationRelationshipName = "indskr_indskr_therapeuticarea_indskr_iopresentat";
            public const string ProductRelationshipName = "indskr_product_indskr_therapeuticarea";
            public const string CreatedOn = "createdon";


        }

        internal struct TimeOffRequest
        {
            public const string EntityName = "indskr_timeoffrequest";
            public const string IsAllDayEvent = "indskr_isalldayevent";
            public const string EventLength = "indskr_eventlength";
            public const string EndDate = "indskr_endtime";
            public const string StartDate = "indskr_starttime";
            public const string OwnerId = "ownerid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string IsAutoApproved = "indskr_isautoapproved";
            public const string PreviousStatus = "indskr_previousstatus";
            public const string ModifiedBy = "modifiedby";
            public const string AppointmentId = "indskr_appointmentid";
            public const string Duration = "indskr_durationcalc";
            public const string ActivityLength = "indskr_eventlength";
            public const string TimeoffRequestId = "indskr_timeoffrequestid";
            public const string DelegateId = "indskr_delegate";



        }

        internal struct DiseaseState
        {
            public const string EntityName = "indskr_diseasestate";
            public const string DiseaseStateId = "indskr_diseasestateid";
            public const string UserRelationShip = "indskr_indskr_diseasestate_systemuser";
            public const string CampaignRealtionship = "indskr_campaign_indskr_diseasestate";
            public const string PresentationRelationship = "indskr_indskr_diseasestate_indskr_iopresentation";

        }

        internal struct AccompaniedUsers
        {
            public const string EntityName = "indskr_accompanieduser";
            public const string AccompaniedUserID = "indskr_accompanieduserid";
            public const string User = "indskr_user";
            public const string Appointment = "indskr_appointment";
            public const string Email = "indskr_emailid";
            public const string JoinStatus = "indskr_joinstatus";
        }

        internal struct AsmtCategoryBUAssociation
        {
            public const string EntityName = "indskr_asmtcategorybuassociation";
            public const string AssesmentCategory = "indskr_assessmentcategory";
            public const string BusinessUnit = "indskr_businessunit";
        }

        internal struct AssessmentMetrics
        {
            public const string EntityName = "indskr_assessmentmetrics";
            public const string CoachingReport = "indskr_coachingreport";
            public const string Category = "indskr_category";
            public const string Measure = "indskr_measure";
            public const string RatingScale = "indskr_coachingratingscale";
        }

        internal struct CoachingMeeting
        {
            public const string EntityName = "indskr_coachingmeeting";
            public const string CoachingReport = "indskr_coachingreport_v2";
            public const string Meeting = "indskr_appointment";
        }

        internal struct CoachingReport
        {
            public const string EntityName = "indskr_coachingreport";
            public const string CoachingFor = "indskr_coachingfor";
            public const string PeriodStartDate = "indskr_periodstartdate";
            public const string PeriodEndDate = "indskr_periodenddate";
            public const string Owner = "ownerid";
            public const string Name = "indskr_name";
            public const string Project = "indskr_project";
            public const string Template = "indskr_coachingtemplateid";
            public const string CoachingPlan = "indskr_coachingplan";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
        }

        internal struct CoachingGoals
        {
            public const string EntityName = "indskr_coachinggoals";
            public const string Name = "indskr_name";
            public const string CoachingFor = "indskr_coachingfor";
            public const string CoachingPlan = "indskr_coachingplan";
            public const string CompletedCoachings = "indskr_completedcoachings";
            public const string PercentCompleted = "indskr_coachingcompletion";
            public const string Owner = "ownerid";
        }

        internal struct CoachingPlan
        {
            public const string EntityName = "indskr_coachingplan";
            public const string Id = "indskr_coachingplanid";
            public const string Project = "indskr_project";
            public const string Team = "indskr_team";
            public const string PeriodStartDate = "indskr_start";
            public const string PeriodEndDate = "indskr_end";
            public const string PercentCoachingCompleted = "indskr_percentcoachingcompleted";
            public const string PercentTimeRemaining = "indskr_percenttimeremaining";
            public const string TotalCoachingCompleted = "indskr_totalcoachingcompleted";
            public const string TotalTeamCoachingGoal = "indskr_totalteamcoachinggoal";
            public const string TotalCoaches = "indskr_totalcoaches";
            public const string UserCoachingGoal = "indskr_usercoachinggoal";
            public const string Name = "indskr_name";
            public const string Recall = "indskr_recall";
            public const string Distribute = "indskr_distribute";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
        }

        internal struct CoachingUser
        {
            public const string EntityName = "indskr_indskr_coachingplan_systemuser";
            public const string CoachingPlan = "indskr_coachingplanid";
            public const string User = "systemuserid";
        }

        internal struct Measure
        {
            public const string EntityName = "indskr_assessmentmeasure";
            public const string RatingScale = "indskr_assessmentratingscale";
            public const string CoachingMeasure = "indskr_assessmentmeasureid";
            public const string Name = "indskr_name";
        }


        internal struct OrganizationSetting
        {
            public const string EntityName = "indskr_organizationsetting";
            public const string Name = "indskr_name";
            public const string CallPlanGracePeriod = "indskr_callplancompletiongraceperiod";
            public const string Status = "statecode";
            public const string UpdateExistingCallPlans = "indskr_updateexisting";
            public const string RecordMissingMessage = "Organization setting is missing. Please contact your system administrator.";
            public const string ActiveRecordsMessage = "Please use the existing organization settings.";
            public const string ShipmentNumberPrefix = "indskr_shipmentprefix";
            public const string ShipmentNumberSuffix = "indskr_shipmentsuffix";
            public const string OrderIdPrefix = "indskr_orderidprefix";
            public const string OrderIdSuffix = "indskr_orderidsuffix";
            public const string TimeOffRequest = "indskr_timeoffrequest";
            public const string ScientificPlanGracePeriod = "indskr_scientificplangraceperiod";
            public const string CustomerCallPlan = "indskr_customercallplan";

        }

        internal struct SegmentCallPlan
        {
            public const string EntityName = "indskr_cycleplan";
            public const string Name = "indskr_name";
            public const string GracePeriodDate = "indskr_graceperioddate";
            public const string PeriodStartDate = "indskr_startdate";
            public const string PeriodEndDate = "indskr_enddate";
            public const string StatusReason = "statuscode";
            public const string ID = "indskr_cycleplanid";
            public const string CreatedBy = "createdby";
            public const string Product = "indskr_productid";
            public const string EngagementCalculation = "indskr_engagementcalculation";
            internal struct EngagementCalculationValues
            {
                public const int Meeting = 548910000;
                public const int PhoneCall = 548910001;
                public const int MeetingsAndPhoneCalls = 548910002;
            }

        }

        internal struct UserShipmentAllocation
        {
            public const string EntityName = "indskr_usershipmentallocation_v2";
            public const string Name = "indskr_name";
            public const string ShipmentNumber = "indskr_shipmentnumber";
            public const string ShipmentNumberAuto = "indskr_shipmentnumberauto";
            public const string NamePrefix = "Shipment Number ";
            public const string User = "indskr_user";
            public const string AllocationShipment = "indskr_allocationshipment";
            public const string QuantityReceived = "indskr_quantityreceived";
            public const string ShipmentStatus = "indskr_shipmentstatus";
            public const string ShippedBy = "indskr_shippedby";
            public const string TransferType = "indskr_transfertype";
            public const string QuantityShipped = "indskr_quantityshipped";
            public const string Lot = "indskr_lot";
            public const string SKUProduct = "indskr_skuproduct";
            internal struct ShipmentStatuses
            {
                public const int Shipped = 548910000;
                public const int Receipted = 548910001;
            }
            internal struct TransferTypes
            {
                public const int AllocationTransfer = 548910000;
                public const int UserShipmentAllocation = 548910001;
            }
        }

        internal struct UserAllocationQuantity
        {
            public const string EntityName = "indskr_userallocationquantity";
            public const string Name = "indskr_name";
            public const string User = "indskr_user";
            public const string LOT = "indskr_lot";
            public const string TotalQuantityDropped = "indskr_totalquantitydropped";
            public const string TotalQuantityRecieved = "indskr_totalquantityrecieved";
            public const string TotalQuantityRemaining = "indskr_totalquantityremaining";
            public const string TotalQuantityTransferred = "indskr_totalquantitytransferred";
        }

        internal struct AllocationShipment
        {
            public const string EntityName = "indskr_allocationshipment_v2";
            public const string EntityId = "indskr_allocationshipment_v2id";
            public const string Name = "indskr_name";
            public const string LOT = "indskr_lot";
        }

        internal struct SampleAllocationSKU
        {
            public const string EntityName = "indskr_activitysampledropped";
            public const string Name = "indskr_name";
            public const string SampleRequest = "indskr_sampledropid";

        }
        internal struct SampleRequest
        {
            public const string EntityName = "indskr_sampledrop";
            public const string SampleRequestCount = "indskr_samplesdroppedcount";
            public const string OrderId = "indskr_orderid";
            public const string OrderIdAuto = "indskr_orderidauto";
            public const string SubjectPrefix = "Order ";
            public const string Subject = "subject";
            public const string OrderIdDisplayName = "Order ID";
            public const string SubjectDisplayName = "Subject";
            public const string CreateCompletedRequest = "indskr_createcompletedrequest";
            public const string StateCode = "statecode";
            public const string StatusCode = "statuscode";
            public const string ParentOrder = "indskr_parentorderid";
            public const string Appointment = "indskr_appointmentid";

        }

        internal struct TempData
        {
            public const string EntityName = "indskr_tempdata";
            public const string Data = "indskr_data";
            public const string LinkToNextRecord = "indskr_nextrecord";

        }

        internal struct AssessmentRatingScale
        {
            public const string EntityName = "indskr_assessmentratingscale";
            public const string Status = "statecode";

        }

        internal struct BURoutingConfigMap
        {
            public const string EntityName = "indskr_buroutingconfigmap";
            public const string BusinessUnit = "indskr_businessunit";
            public const string RoutingConfiguration = "indskr_routingconfiguration";
        }

        internal struct SampleAllocationProfile
        {
            public const string EntityName = "indskr_sampleproductdropconfiguration";
            public const string Name = "indskr_name";
            public const string SampleProduct = "indskr_sampleproductid";
            public const string StratDate = "indskr_startdate";
            public const string EndDate = "indskr_enddate";
            public const string Quantity = "indskr_eligibilitylimit";
            public const string AllocationFrequency = "indskr_periodduration";
            public const string AllocationFrequencyUnit = "indskr_perioddurationunit";
            public const string EnableDirectShip = "indskr_EnableDirectShip";
        }

        internal struct CustomerSampleAllocation
        {
            public const string EntityName = "indskr_customersampleproduct";
            public const string ID = "indskr_customersampleproductid";
            public const string SampleProductDropConfiguration = "indskr_sampleproductdropconfigurationid";
            public const string SampleProduct = "indskr_sampleproductid";
            public const string StratDate = "indskr_startdate";
            public const string EndDate = "indskr_enddate";
            public const string EligibilityLimit = "indskr_eligibilitylimit";
            public const string TotalSamplesDropped = "indskr_totalsamplesdropped";
            public const string TotalSamplesRemaining = "indskr_totalsamplesremaining";
            public const string CustomerSegmentation = "indskr_productratingid";
            public const string Name = "indskr_name";
            public const string DirectShip = "indskr_enabledelivery";

        }

        internal struct SchedullingPattern
        {
            public const string EntityName = "indskr_schedulingpattern";
        }

        internal struct SchedulingConfigurationField
        {
            public const string EntityName = "indskr_schedulingconfigurationfield";
            public const string SequenceOrder = "indskr_sequenceorder";
            public const string SchedulingConfigurations = "indskr_schedulingconfiguration";
            public const string Field = "indskr_schedulingfield";

        }

        internal struct CustomerSegmentLabel
        {
            public const string EntityName = "indskr_customersegmentation";
            public const string Rank = "indskr_rank";
        }

        internal struct CoachingTemplateMeasure
        {
            public const string EntityName = "indskr_coachingtemplatemeasure";
            public const string Name = "indskr_name";
            public const string CoachingTemplate = "indskr_coachingtemplate";
            public const string Category = "indskr_category";
            public const string Measure = "indskr_measure";
        }

        internal struct CoachingTemplate
        {
            public const string EntityName = "indskr_coachingtemplate";
            public const string PositionGroup = "indskr_positiongroup";
            public const string CoachingTemplatePositionGroupRelationship = "indskr_coachingtemplate_positiongroups_CoachingTemplate";
        }

        internal struct PositionGroup
        {
            public const string EntityName = "indskr_positiongroups";
            public const string CoachingTemplate = "indskr_coachingtemplate";
            internal struct Relationships
            {
                internal struct PositionGroupToPosition
                {
                    public const string Name = "indskr_indskr_positiongroups_position_N2N";
                    public const string RelationshipEntityName = "indskr_indskr_positiongroups_position_n2n";
                }
                internal struct PositionGroupToPresentation
                {
                    public const string Name = "indskr_positiongroups_iopresentation";

                }
            }
        }

        internal struct UserOnHandLotQuantity
        {
            public const string EntityName = "indskr_userallocationquantity";
            public const string User = "indskr_user";
            public const string Owner = "ownerid";
            public const string LOT = "indskr_lot";
            public const string TotalQantityReceived = "indskr_totalquantityrecieved";
            public const string TotalQuantityDropped = "indskr_totalquantitydropped";
            public const string TotalQuantityRemaining = "indskr_totalquantityremaining";
            public const string TotalQuantityTransferred = "indskr_totalquantitytransferred";
            public const string ID = "indskr_userallocationquantityid";
        }

        internal struct AllocationRulesProfile
        {
            public const string EntityName = "indskr_allocationrulesprofile";
            public const string CustomerSegment = "indskr_customersegment";
            public const string EligibleAddressCheck = "indskr_eligibleaddresscheck";
        }
        internal struct ActivityMimeAttachment
        {
            public const string EntityName = "activitymimeattachment";
            public const string Item = "objectid";
        }
        internal struct Note
        {
            public const string EntityName = "annotation";
            // public const string Item = "objectid";
        }
        internal struct EmailAttachment
        {
            public const string EntityName = "indskr_emailattachment";
            public const string Status = "statecode";
            public const string Email = "indskr_email";
            public const string AttachmentThumbnailTitle = "indskr_ckmtitle";
            public const string AttachmentThumbnailImageURL = "indskr_ckmthumbnailurl";
            public const string AttachmenthumbnailAssetURL = "indskr_ckmasseturl";
            public const string Document = "indskr_document";
            public const string Resource = "indskr_resource";
            public const string Title = "indskr_name";
        }

        internal struct CustomerContent
        {
            public const string EntityName = "indskr_customercontentmatch";
            public const string Customer = "indskr_customer";
            public const string Document = "indskr_document";
            public const string Resource = "indskr_resource";
            public const string Presentation = "indskr_presentation";
            public const string ContentType = "indskr_contenttype";
            public const string StateCode = "statecode";
        }

        internal struct CustomerContentMatchingVE
        {
            public const string EntityName = "indskr_customercontentmatchesve";
            public const string Id = "indskr_customercontentmatchesveid";
            public const string Name = "indskr_name";
            public const string Customer = "indskr_customer";
            public const string Document = "indskr_document";
            public const string Resource = "indskr_resource";
            public const string Presentation = "indskr_presentation";
            public const string ContentType = "indskr_contenttype";
            public const string StateCode = "statecode";
        }

        internal struct MatchedRuleGroup
        {
            public const string EntityName = "indskr_matchedrulegroup";
            public const string CustomerContent = "indskr_customercontent";
            public const string ContentGroup = "indskr_contentgroup";
            public const string ContentMatchingRule = "indskr_contentmatchingrule";
        }

        internal struct ContentGroup
        {
            public const string EntityName = "indskr_contentgroup";
            public const string Status = "indskr_status";
        }

        internal struct ContentMatchingRule
        {
            public const string EntityName = "indskr_contentmatchingrule";
            public const string Query = "indskr_query";
        }

        public struct Case
        {
            public const string EntityName = "incident";
            public const string ID = "incidentid";
            public const string OmnipresenceStage = "indskr_omnipresencestage";
            public const string Owner = "ownerid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string OwningUser = "owninguser";

            public const string Title = "title";
            public const string CaseOriginCode = "caseorigincode";
            public const string ExpertCategory = "indskr_expertcategory";
            public const string ProductId = "productid";
            public const string TherapeuticArea = "indskr_therapeuticarea";
            public const string CustomerEmail = "indskr_customeremail";
            public const string CustomerId = "customerid";
            public const string Account = "indskr_account";
            public const string TicketNumber = "ticketnumber";
            public const string IntentPrediction = "indskr_intentprediction";
            public const string IsEscalated = "isescalated";
            public const string ModifiedOn = "modifiedon";
            public const string CreatedOn = "createdon";
            public const string PhoneCall = "indskr_phonecallid";



            public struct StatusReasons
            {
                public const int InProgress = 1;
                public const int OnHold = 2;
                public const int WaitingforDetails = 3;
                public const int Researching = 4;
                public const int ProblemSolved = 5;
                public const int InformationProvided = 1000;
                public const int Cancelled = 6;
                public const int Merged = 2000;
            }

            public enum StatusReasonValues
            {
                InProgress = 548910000,
                OnHold = 548910001,
                WaitingforDetails = 548910002,
                Researching = 548910003,
                ProblemSolved = 548910004,
                InformationProvided = 548910005,
                Cancelled = 548910005,
                Merged = 548910005
            }
        }

        public struct ExpertCategories
        {
            public const string EntityName = "indskr_expertcategories";
            public const string ID = "indskr_expertcategoriesid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string Name = "indskr_name";
            public const string Category = "indskr_category";
            public const string CreatedOn = "createdon";

            public struct Categories
            {
                public const string Case = "548910000";
            }
        }


        public struct CaseAssignmentHistory
        {
            public const string EntityName = "indskr_caseassignmenthistory";
            public const string Case = "indskr_case";
            public const string Assignee = "indskr_assignee";
            public const string AssignedOn = "createdon";
            public const string AssignedBy = "indskr_assignedby";
            public const string CaseStatus = "indskr_casestatus";
            public const string ActiveCaseStage = "indskr_activestage";
        }

        public struct OpportunityProcessHistory
        {
            public const string EntityName = "indskr_opportunitypocesshistory";
            public const string Name = "indskr_name";
            public const string OpportunityStatus = "indskr_opportunitystatus";
            public const string Opportunity = "indskr_opporunityid";
            //public const string CaseStatus = "indskr_casestatus";
            public const string ActiveProcessStage = "indskr_activestageid";
        }
        public struct Publication
        {
            public const string EntityName = "indskr_publications";
            public const string ID = "indskr_publicationsid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";

            public struct Relationships
            {
                public struct PublicationToContact
                {
                    public const string Name = "indskr_contact_indskr_publications";
                    public const string EntityName = "indskr_contact_indskr_publications";
                    public const string ContactId = "contactid";
                    public const string PublicationId = "indskr_publicationsid";
                }
            }
        }

        public struct ResearchWork
        {
            public const string EntityName = "ind_researchwork";
            public const string ID = "ind_researchworkid";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";

            public struct Relationships
            {
                public struct ResearchWorkToContact
                {
                    public const string Name = "indskr_contact_indskr_researchworks";
                    public const string EntityName = "indskr_contact_indskr_researchworks";
                    public const string ContactId = "contactid";
                    public const string ResearchWorkId = "ind_researchworkid";
                }
            }
        }

        internal struct Speciality
        {
            public const string EntityName = "indskr_lu_specialty";

            internal struct Relationships
            {

                internal struct Document
                {
                    public const string Name = "indskr_iodocument_specialty";
                    public const string RelationshipEntityName = "indskr_iodocument_specialty";
                }
                internal struct Presentation
                {
                    public const string Name = "indskr_iopresentation_specialty";
                    public const string RelationshipEntityName = "indskr_iopresentation_specialty";
                }
                internal struct Resource
                {
                    public const string Name = "indskr_ioresource_specialty";
                    public const string RelationshipEntityName = "indskr_ioresource_specialty";
                }

            }
        }

        internal struct CallObjective
        {
            public const string EntityName = "indskr_callobjective";
            public const string ID = "indskr_callobjectiveid";
            public const string PhoneCall = "indskr_phonecallid";
        }

        internal struct SearchEntity
        {
            public const string EntityName = "indskr_searchentity";
            public const string ID = "indskr_searchentityid";
            public const string FormID = "indskr_mobileappformid";
            public const string RelatedEntity = "indskr_relatedentity";
        }

        #region Fetch Xml for Customer Allocation Logic App
        internal struct CustomerAllocationFetchXml
        {
            public const string MainXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                                              <entity name='indskr_productrating'>
                                                <attribute name='indskr_product' />
                                                <attribute name='indskr_contact' />
                                                <attribute name='indskr_productratingid' />
                                                <order attribute='indskr_productratingid' descending='false' />    
                                                <filter type='and'>
                                               <condition attribute='indskr_product' operator='eq' uitype='product' value='[product_id]' />                                                 
                                                [segment_condition]
                                               </filter>
                                                  [segment_status_filter]
                                                <link-entity name='contact' from='contactid' to='indskr_contact' link-type='inner' alias='customer'>
                                                  <attribute name='fullname' />
                                                  <filter type='and'>
                                                    <condition attribute='statecode' operator='eq' value='0' />
                                                  </filter>
                                                  [speciality_link_entity]
                                                  [country_link_entity]
                                                  </link-entity>                                               
                                                <link-entity name='product' from='productid' to='indskr_product' visible='false' link-type='outer' alias='product'>
                                                  <attribute name='name' />
                                                </link-entity>
                                              </entity>
                                            </fetch>";
            public const string ProductCondition = @"<condition attribute='indskr_product' operator='eq' uitype='product' value='[product_id]' />";
            public const string SegmentCondition = @"<condition attribute='indskr_segmentation' operator='eq' uitype='indskr_customersegmentation' value='[segment_id]' />";
            public const string SpecialityLinkEntity = @"<link-entity name='indskr_customerspecialty' from='indskr_customerid' to='contactid' link-type='inner' alias='bt'>
                                                            <filter type='and'>
                                                             <condition attribute='indskr_specialtyid' operator='in'>
                                                             [speciality_values]
                                                               </condition>
                                                              <condition attribute='statecode' operator='eq' value='0' />
                                                            </filter>
                                                          </link-entity>";
            public const string CountryLinkEntity = @"<link-entity name='indskr_indskr_customeraddress_v2' from='indskr_customer' to='contactid' link-type='inner' alias='bu'>
                                                        <filter type='and'>
                                                          <condition attribute='statecode' operator='eq' value='0' />
                                                         [sampling_eligible_condition]
                                                        </filter>
                                                        <link-entity name='indskr_address' from='indskr_addressid' to='indskr_address' link-type='inner' alias='bv'>
                                                          <filter type='and'>
                                                           <condition attribute='indskr_country_lu' operator='in'>
                                                                  [country_values]
                                                          </condition>
                                                          <condition attribute='statecode' operator='eq' value='0' />
                                                          </filter>
                                                        <link-entity name='indskr_lu_country' from='indskr_lu_countryid' to='indskr_country_lu' link-type='inner' alias='be'>
                                                        <filter type='and'>
                                                         <condition attribute='statecode' operator='eq' value='0' />
                                                         </filter>
                                                          </link-entity>
                                                         </link-entity>
                                                        </link-entity>";
            public const string SamplingEligibleCondition = @"<condition attribute='indskr_samplingeligible' operator='eq' value='1' />";
            public const string CountryValue = @"<value uitype='indskr_lu_country'>[country_value]</value>";
            public const string SpecialityValue = @"<value uitype='indskr_lu_specialty'>[speciality_value]</value>";
            public const string SegmentStatusFilter = @" <link-entity name='indskr_customersegmentation' from='indskr_customersegmentationid' to='indskr_segmentation' link-type='inner' alias='segment'>
                                                         <filter type='and'>
                                                         <condition attribute='statecode' operator='eq' value='0' />
                                                         </filter>
                                                         </link-entity>";

        }
        #endregion

        internal struct TimeOffRequestsFetchXml
        {
            public const string MainXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
  <entity name='indskr_timeoffrequest'>
    <attribute name='indskr_timeoffrequestid' />
    <attribute name='indskr_endtime' />
    <attribute name='indskr_starttime' />
    <attribute name='indskr_delegate' />
    <attribute name='ownerid' />
    <order attribute='indskr_delegate' descending='false' />
    <filter type='and'>
      <condition attribute='ownerid' operator='eq' uitype='systemuser' value='{0}' />
      <condition attribute='statuscode' operator='eq' value='100000004' />
    </filter>
  </entity>
</fetch>";
        }

        internal struct ActualRevenueCalculationFetchXML
        {
            public const string MainXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
  <entity name='salesorder'>
    <attribute name='totalamount' />
    <filter type='and'>
      <condition attribute='statecode' operator='eq' value='3' />
      <condition attribute='opportunityid' operator='eq' uiname='ActualRevenue Calculation' uitype='opportunity' value='{0}' />
      <condition attribute='transactioncurrencyid' operator='eq' uiname='Rupee' uitype='transactioncurrency' value='{1}' />
    </filter>
  </entity>
</fetch>";
        }


        public struct BrickPosition
        {
            public const string EntityName = "indskr_bricksposition";
            public const string Name = "indskr_name";
            public const string Brick = "indskr_brick";
            public const string Position = "indskr_position";
            public const string ValidFrom = "indskr_validfrom";
            public const string ValidTo = "indskr_validto";
        }

        public struct Brick
        {
            public const string EntityName = "indskr_brick";
            public const string Id = "indskr_brickid";
            public const string Name = "indskr_name";
        }

        internal struct MarketScan
        {
            public const string EntityName = "indskr_marketscan";
            public const string Name = "indskr_name";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string Date = "indskr_date";
            public const string ExpiryDate = "indskr_expirydate";

            public struct StatusReasons
            {
                public const int Open = 1;
                public const int Inactive = 2;
                public const int Completed = 548910001;
            }

            internal struct Relationships
            {
                internal struct MarketScanContact
                {
                    public const string Name = "indskr_marketscan_contact";
                    public const string RelationshipEntityName = "indskr_marketscan_contact";
                }
                internal struct MarketScanProduct
                {
                    public const string Name = "indskr_marketscan_product";
                    public const string RelationshipEntityName = "indskr_marketscan_product";
                }
                internal struct MarketScanTherapeuticArea
                {
                    public const string Name = "indskr_marketscan_indskr_therapeuticarea";
                    public const string RelationshipEntityName = "indskr_marketscan_indskr_therapeuticare";
                }
                internal struct MarketScanCustomerSegment
                {
                    public const string Name = "indskr_marketscan_indskr_customersegment";
                    public const string RelationshipEntityName = "indskr_marketscan_indskr_customersegment";
                }

            }
        }

        internal struct MarketScanCustomerData
        {
            public const string EntityName = "indskr_marketscancustomerdata";
            public const string Name = "indskr_name";
            public const string Product = "indskr_product";
            public const string Rx = "indskr_rx";
            public const string Customer = "indskr_customer";
            public const string MarketScan = "indskr_marketscan";

        }

        internal struct ExpertEngagementPlan
        {
            public const string EntityName = "indskr_expertengagementplan";
            public const string Name = "indskr_name";
            public const string FunctionalArea = "indskr_functionalarea";
            public const string Year = "indskr_yearoption";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
        }

        internal struct MeasureBoard_KPI
        {
            public const string EntityName = "indskr_measureboardkpi";
            public const string MeasureBoardKPI = "indskr_measureboardkpi";
        }

        internal struct ExpertEngagementRequest
        {
            public const string EntityName = "indskr_expertengagementrequest";
            public const string Name = "indskr_name";
            public const string Year = "indskr_year";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string SubmittedBy = "indskr_submittedby";

            public struct StatusReasons
            {
                public const int Draft = 1;
                public const int Submitted = 548910000;
                public const int InReview = 548910001;
                public const int Approved = 548910002;
                public const int PendingApproval = 548910003;
                public const int ConditionallyApproved = 548910004;
                public const int Cancelled = 548910005;
                public const int Rescheduled = 548910006;
            }
        }

        internal struct ExpertEngagementRequestActivity
        {
            public const string EntityName = "indskr_expertengagementrequestactivity";
            public const string Name = "indskr_name";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";

            public struct StatusReasons
            {
                public const int New = 1;
                public const int Inactive = 2;
                public const int Contracted = 548910000;
                public const int Amend = 548910001;
                public const int Approved = 548910002;
            }
        }

        internal struct NeedsAssessmentLineItem
        {
            public const string EntityName = "indskr_needsassessmentlineitem";
            public const string BusinessJustification = "indskr_businessjustificationid";
            public const string ExpectedOutcome = "indskr_expectedoutcomeid";
            public const string ExpertRequirementReason = "indskr_expertrequirementreasonid";
            public const string RequestedQuantityReason = "indskr_requestedquantityreasonid";
            public const string ExternalId = "indskr_externalid";
            public const string OtherAdditionalCharacteristic = "indskr_otheradditionalcharacteristic";
            public const string EngagementType = "indskr_engagementtype";
            public const string Region = "indskr_region";
            public const string Company = "indskr_company";
            public const string GlobalArea = "indskr_globalarea";
            public const string BusinessArea = "indskr_businessarea";
            public const string FunctionalArea = "indskr_functionalarea";
            public const string HCPFaciltyRequired = "indskr_hcpfacilityrequired";
            public const string LevelOfEducation = "indskr_levelofeducation";
            public const string AdditionalCharacteristics = "indskr_additionalcharacteristics";
            public const string ReputationalProminence = "indskr_reputationalprominence";
            public const string ThirdPartyIntermediary = "indskr_thirdpartyintermediary";
            public const string YearsOfExperience = "indskr_yearsofexperience";
            public const string NumberOfActivities = "indskr_numberofactivities";
            public const string NumberOfHCPsPerService = "indskr_numberofhcpsperservice";
            public const string NumberOfServices = "indskr_numberofservices";
            public const string Year = "indskr_yearoption";
            public const string ExpertEngagementPlan = "indskr_expertengagementplan";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";

            public struct StatusReasons
            {
                public const int Draft = 1;
                public const int Submitted = 548910000;
                public const int InReview = 548910001;
                public const int Approved = 548910002;
            }
        }
        internal struct AdminAlerts
        {
            public const string EntityName = "indskr_adminalerts";
            public const string Name = "indskr_name";
            public const string Status = "statecode";
            public const string StatusReason = "statuscode";
            public const string CreatedBy = "createdby";

            internal struct StatusReasons
            {
                public const int Published = 548910004;
                public const int Failure = 548910000;
                public const int PartialFailure = 548910002;
            }
        }

        internal struct ActivityType
        {
            public const string EntityName = "indskr_activitytype";
            public const string ID = "indskr_activitytypeid";
            public const string ActivityTypeName = "indskr_name";
            public const string ActivitySubType = "indskr_activitysubtype";
            public const string OmnipresenceActivity = "indskr_omnipresenceactivity";
            public const string Default = "indskr_default";
            public const string Mandatory = "indskr_mandatory";
        }

        internal struct ActivitySubType
        {
            public const string EntityName = "indskr_activitysubtype";
            public const string ID = "indskr_activitysubtypeid";
            public const string ActivitySubTypeName = "indskr_name";
            public const string ActivityType = "indskr_activitytype";
            public const string OmnipresenceActivity = "indskr_omnipresenceactivity";
            public const string Default = "indskr_default";
            public const string ProductMandatory = "indskr_productmandatory";
            public const string Mandatory = "indskr_mandatory";
        }
    }
}
