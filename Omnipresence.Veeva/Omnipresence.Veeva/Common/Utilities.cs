﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Net.Http;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Newtonsoft.Json.Linq;
using Microsoft.Xrm.Sdk;
using System;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Crm.Sdk.Messages;

namespace Omnipresence.Veeva
{
    public class Utilities
    {
        #region Singleton Implementation

        //Hide the default constructor
        private Utilities()
        {
            ExtEntities = new ExtensionEntities();
            EntityBulkOprs = new EntityBulkOperations();


            #region Initialize Address1FieldsOfAddresFields

            Address1FieldsOfAddresFields = new Dictionary<string, string>();

            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Name, EntityAttrs.Address1Fields.Address1_Name);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.PostBoxNo, EntityAttrs.Address1Fields.Address1_PostOfficeBox);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Street1, EntityAttrs.Address1Fields.Address1_Line1);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Street2, EntityAttrs.Address1Fields.Address1_Line2);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Street3, EntityAttrs.Address1Fields.Address1_Line3);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.City, EntityAttrs.Address1Fields.Address1_City);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.PostalCode, EntityAttrs.Address1Fields.Address1_PostalCode);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.State, EntityAttrs.Address1Fields.Address1_StateProvince);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Country, EntityAttrs.Address1Fields.Address1_Country);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Composite, EntityAttrs.Address1Fields.Address1_Composite);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.PrimaryContact, EntityAttrs.Address1Fields.Address1_PrimaryContactName);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Phone1, EntityAttrs.Address1Fields.Address1_Telephone1);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Phone2, EntityAttrs.Address1Fields.Address1_Telephone2);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Fax1, EntityAttrs.Address1Fields.Address1_Fax);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Latitude, EntityAttrs.Address1Fields.Address1_Latitude);
            Address1FieldsOfAddresFields.Add(EntityAttrs.Address.Longitude, EntityAttrs.Address1Fields.Address1_Longitude);

            #endregion

        }

        private static Utilities _inst = default(Utilities);

        public static Utilities Inst
        {
            get
            {
                if (_inst == null)
                    _inst = new Utilities();

                return _inst;
            }
        }

        #endregion

        /// <summary>
        /// Extension Entities
        /// </summary>
        public ExtensionEntities ExtEntities { get; }

        public Dictionary<string, string> Address1FieldsOfAddresFields;

        /// <summary>
        /// Entity Bulk Operations
        /// </summary>
        public EntityBulkOperations EntityBulkOprs { get; }

        public T GetAttributeValue<T>(Entity targetEntity, string attributeName)
        {
            try
            {
                object result = default(T);
                if (targetEntity != null && targetEntity.Attributes.Contains(attributeName))
                {
                    result = targetEntity.Attributes[attributeName];
                }

                return (T)result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public T GetAttributeValue<T>(Entity targetEntity, Entity savedEntity, string attributeName)
        {
            try
            {
                object result = default(T);
                if (targetEntity != null && targetEntity.Attributes.Contains(attributeName))
                {
                    result = targetEntity.Attributes[attributeName];
                }
                else if (savedEntity != null && savedEntity.Attributes.Contains(attributeName))
                {
                    result = savedEntity.Attributes[attributeName];
                }

                return (T)result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetOptionSetTextValue(string entityName, string fieldName, IOrganizationService service, OptionSetValue resultStatus)
        {
            var attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = fieldName
            };

            var attributeResponse = (RetrieveAttributeResponse)service.Execute(attributeRequest);
            var attributeMetadata = (EnumAttributeMetadata)attributeResponse.AttributeMetadata;

            var optionList = (from o in attributeMetadata.OptionSet.Options
                              select new { Value = o.Value, Text = o.Label.UserLocalizedLabel.Label }).ToList();

            var resultStatusText = optionList.Where(o => o.Value == resultStatus.Value)
                        .Select(o => o.Text)
                        .FirstOrDefault();
            return resultStatusText;
        }

        public void SetAttributeValue(Entity entity, string attributeName, object value)
        {
            try
            {
                // Refactored to remove unnecessary code;
                entity[attributeName] = value;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public void UpdateEntityAttribute(ITracingService trac, IOrganizationService orgService, string entityName, Guid entityID, string attributeName, object attributeValue)
        {
            // Refactored to just call the other function.  These were identical but this one accepts the tracing service and never used it.
            UpdateEntityAttribute(orgService, entityName, entityID, attributeName, attributeValue);
        }

        public void UpdateEntityAttribute(IOrganizationService orgService, string entityName, Guid entityID, string attributeName, object attributeValue)
        {
            try
            {
                // Refactored to remove unnecessary retrieve when updating
                Entity targetEntity = new Entity(entityName, entityID);

                //Update the email address
                Utilities.Inst.SetAttributeValue(targetEntity, attributeName, attributeValue);

                //Push the updated entity to the server
                orgService.Update(targetEntity);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Entity> GetRelatedEntities(IOrganizationService orgService
                                    , string linkFromEntityName, Guid linkFromEntityId, ColumnSet linkFromColumnSet, string relationshipName, string linkToEntityName
                                    , ColumnSet linkToColumnSet)
        {
            // create the query expression object
            var query = new QueryExpression();
            query.EntityName = linkToEntityName;
            query.ColumnSet = linkToColumnSet;

            // create the relationship object
            var relationship = new Relationship();
            relationship.SchemaName = relationshipName;

            // create relationshipQueryCollection Object
            var relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);

            // create the retrieve request object
            var request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;

            //set column to  and the condition for the account

            request.ColumnSet = linkFromColumnSet;
            request.Target = new EntityReference { Id = linkFromEntityId, LogicalName = linkFromEntityName };

            IEnumerable<Entity> result;

            //execute the request
            try
            {
                var response = (RetrieveResponse)orgService.Execute(request);

                result = response.Entity.RelatedEntities[relationship].Entities;
            }
            catch (Exception)
            {
                result = Enumerable.Empty<Entity>();
            }

            return result;
        }

        public Entity GetRecordByUniqueAttribute(IOrganizationService orgService, ITracingService tracService, string entityLogicalName, string uniqueAttributeName,
            string uniqueAttributeValue, ColumnSet columns)
        {
            try
            {
                QueryExpression qe = new QueryExpression(entityLogicalName);
                qe.Criteria.AddCondition(new ConditionExpression(uniqueAttributeName, ConditionOperator.Equal, uniqueAttributeValue));
                qe.ColumnSet = columns;

                // Retrieve the results
                Entity entityRecord = orgService.RetrieveMultiple(qe).Entities.FirstOrDefault();

                return entityRecord;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public EntityCollection GetRecordByUniqueAttribute(IOrganizationService orgService, string entityLogicalName, string uniqueAttributeName,
           string uniqueAttributeValue, ColumnSet columns)
        {
            try
            {
                QueryExpression qe = new QueryExpression(entityLogicalName);
                qe.Criteria.AddCondition(new ConditionExpression(uniqueAttributeName, ConditionOperator.Equal, uniqueAttributeValue));
                qe.ColumnSet = columns;

                // Retrieve the results
                EntityCollection entityRecord = orgService.RetrieveMultiple(qe);

                return entityRecord;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get the current user's time zone 
        /// </summary>
        /// <param name="orgService">The organization service</param>
        /// <param name="entityLogicalName">The entity logical name</param>
        /// <param name="uniqueAttributeName">The unique attribute name</param>
        /// <param name="userId">The user Id</param>
        /// <param name="columns">The columns</param>
        /// <returns>The entity with time zone</returns>
        public EntityCollection GetCurrentUserTimeZone(IOrganizationService orgService, string entityLogicalName, string uniqueAttributeName,
          Guid userId, ColumnSet columns)
        {
            try
            {
                QueryExpression qe = new QueryExpression(entityLogicalName);
                qe.Criteria.AddCondition(new ConditionExpression(uniqueAttributeName, ConditionOperator.Equal, userId));
                qe.ColumnSet = columns;

                // Retrieve the results
                EntityCollection entityRecord = orgService.RetrieveMultiple(qe);

                return entityRecord;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String GetUserFullname(IOrganizationService service, Guid userId)
        {
            String fullname = String.Empty;

            Entity user = service.Retrieve(EntityAttrs.User.EntityName, userId, new ColumnSet(EntityAttrs.User.FullName));
            fullname = user[EntityAttrs.User.FullName].ToString();

            return fullname;
        }

        public UserRoles GetUserRoles(IOrganizationService service, Guid userId)
        {
            UserRoles roles = new UserRoles();

            // Build Query
            QueryExpression query = new QueryExpression("systemuserroles");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);
            query.ColumnSet = new ColumnSet("roleid");

            EntityCollection ec = service.RetrieveMultiple(query);
            foreach (Entity sysRole in ec.Entities)
            {
                Guid roleId = (Guid)sysRole["roleid"];

                if (roleId == EntityAttrs.SecurityRoles.Administrator.Id)
                {
                    roles.IoAdmin = true;
                }
                if (roleId == EntityAttrs.SecurityRoles.Manager.Id)
                {
                    roles.IoManager = true;
                }
                if (roleId == EntityAttrs.SecurityRoles.User.Id)
                {
                    roles.IoUser = true;
                }
            }

            return roles;
        }

        public EntityCollection GetPositionsForUser(IOrganizationService service, string entityId, bool? Primary = null)
        {
            QueryExpression queryExpression = new QueryExpression(EntityAttrs.Position.EntityName);
            queryExpression.ColumnSet = new ColumnSet(EntityAttrs.Position.Id);
            var link = queryExpression.AddLink(EntityAttrs.UserPosition.EntityName, EntityAttrs.Position.Id, EntityAttrs.UserPosition.Position);
            var criteria = new FilterExpression();
            criteria.AddCondition(EntityAttrs.UserPosition.User, ConditionOperator.Equal, entityId);

            if (Primary != null)
                criteria.AddCondition(EntityAttrs.UserPosition.Primary, ConditionOperator.Equal, Primary.Value);
            link.LinkCriteria = criteria;
            link.Orders.Add(new OrderExpression(EntityAttrs.UserPosition.Primary, OrderType.Descending));
            var positions = service.RetrieveMultiple(queryExpression);
            return positions;
        }

        public EntityCollection GetRecordByStatus(IOrganizationService orgService, string entityLogicalName, string uniqueAttributeName,
           int uniqueAttributeValue, ColumnSet columns)
        {
            try
            {
                QueryExpression qe = new QueryExpression(entityLogicalName);
                qe.Criteria.AddCondition(new ConditionExpression(uniqueAttributeName, ConditionOperator.Equal, uniqueAttributeValue));
                qe.ColumnSet = columns;

                // Retrieve the results
                EntityCollection entityRecord = orgService.RetrieveMultiple(qe);

                return entityRecord;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public EntityCollection GetRelatedEntityRecords(IOrganizationService orgService, string entityLogicalName, string attributeName,
           Guid attributeValue, ColumnSet columns)
        {
            try
            {
                QueryExpression qe = new QueryExpression(entityLogicalName);

                qe.Criteria.AddCondition(new ConditionExpression(attributeName, ConditionOperator.Equal, attributeValue));

                qe.ColumnSet = columns;

                // Retrieve the results
                EntityCollection entityRecord = orgService.RetrieveMultiple(qe);

                return entityRecord;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public EntityCollection GetRecordWithoutCondition(IOrganizationService orgService, string entityLogicalName, ColumnSet columns)
        {
            try
            {
                QueryExpression qe = new QueryExpression(entityLogicalName);
                qe.ColumnSet = columns;

                // Retrieve the results
                EntityCollection entityRecord = orgService.RetrieveMultiple(qe);

                return entityRecord;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///  Retrive the local time from the UTC time.
        /// </summary>
        /// <param name="utcTime">UTC Date time which needs to convert to Local DateTime </param>
        /// <param name="timeZoneCode">TimeZoneCode </param>
        /// <param name="service">OrganizationService service</param>
        /// <returns>The local time</returns>
        public DateTime RetrieveLocalTimeFromUTCTime(DateTime utcTime, int? timeZoneCode, IOrganizationService service)

        {
            if (!timeZoneCode.HasValue)
                return DateTime.Now;

            var request = new LocalTimeFromUtcTimeRequest
            {
                TimeZoneCode = timeZoneCode.Value,
                UtcTime = utcTime.ToUniversalTime()
            };

            var response = (LocalTimeFromUtcTimeResponse)service.Execute(request);

            return response.LocalTime;

        }

        /// <summary>
        ///  Retrive the UTC time from the local time.
        /// </summary>
        /// <param name="localTime">Local Date time which needs to convert to UTC DateTime</param>
        /// <param name="timeZoneCode">TimeZoneCode</param>
        /// <param name="service">The OrganizationService</param>
        public DateTime RetrieveUTCTimeFromLocalTime(DateTime localTime, int? timeZoneCode, IOrganizationService service)
        {
            if (!timeZoneCode.HasValue)
                return localTime;

            var request = new UtcTimeFromLocalTimeRequest
            {
                TimeZoneCode = timeZoneCode.Value,
                LocalTime = localTime
            };

            var response = (UtcTimeFromLocalTimeResponse)service.Execute(request);

            return response.UtcTime;
        }

        public T GetEntityAttribute<T>(IOrganizationService orgService, ITracingService tracService, string entityLogicalName, Guid ID, string AttributeLogialName)
        {
            try
            {
                // Retrieve the results
                Entity entityRecord = orgService.Retrieve(entityLogicalName, ID, new ColumnSet(AttributeLogialName));
                T attributeValue = default(T);

                if (entityRecord != null)
                {
                    attributeValue = Utilities.Inst.GetAttributeValue<T>(entityRecord, AttributeLogialName);
                }

                return attributeValue;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Entity GetRandomEntityRecord(IOrganizationService orgService, ITracingService tracService, string entityLogicalName)
        {
            try
            {
                QueryExpression qe = new QueryExpression(entityLogicalName);
                qe.ColumnSet = new ColumnSet(EntityAttrs.CommonAttributes.StatusCode);

                Entity entityRecord = orgService.RetrieveMultiple(qe).Entities.FirstOrDefault();

                return entityRecord;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public EntityCollection GetRandomEntityRecord(IOrganizationService service, string entityLogicalName, List<string> relatedEntityAttribute, List<string> relatedentityValues)
        {
            EntityCollection entityRecord = new EntityCollection();
            try
            {
                QueryExpression query = new QueryExpression(entityLogicalName);
                FilterExpression childFilter = query.Criteria.AddFilter(LogicalOperator.And);
                childFilter.AddCondition(relatedEntityAttribute[0], ConditionOperator.Equal, relatedentityValues[0]);
                childFilter.AddCondition(relatedEntityAttribute[1], ConditionOperator.Equal, relatedentityValues[1]);

                entityRecord = service.RetrieveMultiple(query);
                return entityRecord;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public EntityCollection GetRandomEntityRecord(IOrganizationService service, string entityLogicalName, List<string> relatedEntityAttribute, List<string> relatedentityValues, ColumnSet columnset)
        {
            EntityCollection entityRecord = new EntityCollection();
            try
            {
                QueryExpression query = new QueryExpression(entityLogicalName);
                query.ColumnSet = columnset;
                FilterExpression childFilter = query.Criteria.AddFilter(LogicalOperator.And);
                childFilter.AddCondition(relatedEntityAttribute[0], ConditionOperator.Equal, relatedentityValues[0]);
                childFilter.AddCondition(relatedEntityAttribute[1], ConditionOperator.Equal, relatedentityValues[1]);

                entityRecord = service.RetrieveMultiple(query);
                return entityRecord;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public T DeserializeJSONToObject<T>(string json)
        {
            T deserializedObject;
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            deserializedObject = (T)ser.ReadObject(ms);
            ms.Close();
            return deserializedObject;
        }

        public string SerializeObjectToJSON(object obj)
        {
            DataContractJsonSerializer json = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            XmlDictionaryWriter writer = JsonReaderWriterFactory.CreateJsonWriter(ms);
            json.WriteObject(ms, obj);
            writer.Flush();
            return Encoding.UTF8.GetString(ms.GetBuffer()).Trim().TrimEnd('\0');
        }

        public EntityReference GetOutboundEmailQueue(IOrganizationService service)
        {
            string configName = "Outbound Email Queue";
            EntityReference refQueue = null;

            // Get Configuration
            QueryExpression query = new QueryExpression(EntityAttrs.iOConfiguration.EntityName);
            query.Criteria.AddCondition(EntityAttrs.iOConfiguration.ConfigName, ConditionOperator.Equal, configName);
            query.ColumnSet = new ColumnSet(EntityAttrs.iOConfiguration.ConfigValue);

            EntityCollection ec = service.RetrieveMultiple(query);
            if (ec.Entities.Count > 0)
            {
                Entity config = ec.Entities[0];

                if (config.Contains(EntityAttrs.iOConfiguration.ConfigValue))
                {
                    // Get Email
                    string emailaddress = config[EntityAttrs.iOConfiguration.ConfigValue].ToString().Trim();

                    // Get Queue
                    QueryExpression query2 = new QueryExpression("queue");
                    query2.Criteria.AddCondition("emailaddress", ConditionOperator.Equal, emailaddress);

                    EntityCollection ec2 = service.RetrieveMultiple(query2);
                    if (ec2.Entities.Count > 0)
                    {
                        // Set Reference
                        refQueue = ec2.Entities[0].ToEntityReference();
                    }
                    else
                    {
                        throw new Exception(String.Format("Unable to find Queue for emailaddress [{0}]", emailaddress));
                    }
                }
                else
                {
                    throw new Exception("Unable to find iO Configuration value for 'Outbound Email Queue'");
                }
            }
            else
            {
                throw new Exception("Unable to find iO Configuration 'Outbound Email Queue'");
            }

            return refQueue;
        }

        /// <summary>
        /// To send email for email activity
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="service"></param>
        public void SendEmail(Guid emailId, IOrganizationService service)
        {
            try
            {
                SendEmailRequest sendEmailreq = new SendEmailRequest
                {
                    EmailId = emailId,
                    TrackingToken = "",
                    IssueSend = true
                };
                SendEmailResponse sendEmailresp = (SendEmailResponse)service.Execute(sendEmailreq);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// return entitycollection for passed entity by removing primary field and Id
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <returns></returns>
        public EntityCollection Anonymize(Entity targetEntity)
        {
            EntityCollection entCollection = new EntityCollection();
            try
            {
                //creating new entity instance to set values and remove primary field and Id
                Entity newEntity = (Entity)targetEntity;
                newEntity.Attributes.Remove(EntityAttrs.ActivityParty.ActivityPartyId);
                newEntity.Id = Guid.Empty;
                entCollection.Entities.Add(newEntity);
            }
            catch (Exception ex) { throw; }
            return entCollection;
        }

        /// <summary>
        /// return entitycollection for passed entity collection by removing primary field and Id of included entities
        /// </summary>
        /// <param name="targetEntityCollection"></param>
        /// <returns></returns>
        public EntityCollection Anonymize(EntityCollection targetEntityCollection)
        {
            EntityCollection entCollection = new EntityCollection();
            try
            {
                //loop to get all entities included in collection
                foreach (var entity in targetEntityCollection.Entities)
                {
                    //creating new entity instance to set values and remove primary field and Id
                    Entity newEntity = (Entity)entity;
                    newEntity.Attributes.Remove(EntityAttrs.ActivityParty.ActivityPartyId);
                    newEntity.Id = Guid.Empty;
                    entCollection.Entities.Add(newEntity);
                }
            }
            catch (Exception ex) { throw; }
            return entCollection;

        }

        /// <summary>
        /// Associate record for entities
        /// </summary>
        /// <param name="primaryEntity"></param>
        /// <param name="primaryEntityId"></param>
        /// <param name="relationshipName"></param>
        /// <param name="secondaryEntity"></param>
        /// <param name="secondryEntityId"></param>
        /// <param name="service"></param>
        public void AssociateRecord(ITracingService trace, string primaryEntity, Guid primaryEntityId, string relationshipName, string secondaryEntity, Guid secondryEntityId, IOrganizationService service)
        {
            try
            {
                trace.Trace("In utility association");
                EntityReferenceCollection productAssignEntities = new EntityReferenceCollection();
                productAssignEntities.Add(new EntityReference(secondaryEntity, secondryEntityId));

                service.Associate(primaryEntity, primaryEntityId, new Relationship(relationshipName),
                    productAssignEntities);
                trace.Trace("In utility association Done");
            }
            catch (Exception ex)
            {
                trace.Trace(ex.ToString());
            }
        }

        /// <summary>
        /// Disassociate Records
        /// </summary>
        /// <param name="primaryEntity"></param>
        /// <param name="primaryEntityId"></param>
        /// <param name="relationshipName"></param>
        /// <param name="secondaryEntity"></param>
        /// <param name="secondryEntityId"></param>
        /// <param name="service"></param>
        public void DisassociateRecord(string primaryEntity, Guid primaryEntityId, string relationshipName, string secondaryEntity, Guid secondryEntityId, IOrganizationService service)
        {
            try
            {

                EntityReferenceCollection disassociateRecord = new EntityReferenceCollection();
                disassociateRecord.Add(new EntityReference(secondaryEntity, secondryEntityId));

                //Disassociate the records.
                service.Disassociate(primaryEntity, primaryEntityId, new Relationship(relationshipName),
                    disassociateRecord);
            }
            catch (Exception) { throw; }
        }


        public void CheckInitialization<T>(T field, string fieldName, string entityName)
        {
            try
            {
                Type ty = typeof(T);
                T defaultValue = default(T);

                //if (field == defaultValue)
                //{
                //    throw new Exception("Mandatory field '" + fieldName + "' of entity '" + entityName + "' does not have data.");
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns the field maps of the Address1_xxx fields(Contact/Account) and Address entity fields for the fields available in the given Address entity record
        /// </summary>
        /// <param name="trac"></param>
        /// <param name="addressEntity"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetFieldMapsForAvailableFields(ITracingService trac, Entity addressEntity)
        {
            try
            {

                trac.Trace("Method: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);

                Dictionary<string, string> fieldMaps = new Dictionary<string, string>();

                //Identify the Address1_xxx fields that are to be updated for the related contact entities based on the Address fields that are updated
                foreach (var map in Address1FieldsOfAddresFields)
                {
                    if (addressEntity.Contains(map.Key))
                        fieldMaps.Add(map.Key, map.Value);
                }

                if (fieldMaps.Count > 0)
                    return fieldMaps;
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This function uses stringt and replace all whitespaces between semicolons
        /// </summary>
        /// <param name="localContext"></param>
        /// <param name="targetEntity"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public string ReplaceWhiteSpace(ITracingService trace, IOrganizationService service, Entity targetEntity, string attributeName)
        {
            try
            {
                trace.Trace("Initializing method ReplaceWhiteSpace()");
                iOConfigManager configMan = new iOConfigManager(service, trace);

                Regex regex = new Regex(configMan.GetConfiguration(iOConfigurations.RegularExpression.ReplaceWhiteSpace));
                string alternateNames = regex.Replace(Convert.ToString(targetEntity.Attributes[attributeName]), ";");
                alternateNames = alternateNames.Insert(0, ";");
                alternateNames = alternateNames.Insert(alternateNames.Length, ";");

                return alternateNames;
            }
            catch (Exception)
            {

                throw;
            }


        }

        public EntityMetadata GetEntityMetadata(IOrganizationService service, ITracingService trace, string entityLogicalName, EntityFilters entityFilters)
        {
            try
            {
                trace.Trace("Method: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);

                RetrieveEntityRequest req = new RetrieveEntityRequest();
                req.LogicalName = entityLogicalName;
                req.EntityFilters = entityFilters;

                RetrieveEntityResponse response = service.Execute(req) as RetrieveEntityResponse;

                return response.EntityMetadata;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Call the Logic apps
        /// </summary>
        /// <param name="logicAppUri">The logic app url</param>
        /// <param name="content">The data</param>
        /// <returns>The response</returns>
        public HttpResponseMessage CallLogicApp(string logicAppUri, StringContent content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            using (var client = new HttpClient())
            {
                response = client.PostAsync(logicAppUri, content).Result;
            }

            return response;
        }

        internal string BuildInboundURL(IOrganizationService organizationService, ITracingService tracingService, EntityReference businessUnit, EntityReference routingConfiguration)
        {
            iOConfigManager configMan = new iOConfigManager(organizationService, tracingService);
            string serviceURL = configMan.GetConfiguration("Encryption Service URL");
            string meetingServicesURL = configMan.GetConfiguration("Meeting Services URL");
            string tenantId = configMan.GetConfiguration("Tenant Id");
            string clientId = configMan.GetConfiguration("Client Id");
            string organizationURL = configMan.GetConfiguration("Organization URL");
            string organizationDomain = configMan.GetConfiguration("Organization Domain");
            Guid contactId = GetContactId(organizationService, tracingService, routingConfiguration);
            string inboundURL = string.Empty;

            if (string.IsNullOrEmpty(organizationURL) || string.IsNullOrEmpty(serviceURL) || string.IsNullOrEmpty(tenantId) || string.IsNullOrEmpty(clientId))
                throw new InvalidPluginExecutionException("Missing IO Configuration records to generate Inbound URL");

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(serviceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.KeepAlive = false; // Recommended by Microsoft https://docs.microsoft.com/en-us/powerapps/developer/common-data-service/best-practices/business-logic/set-keepalive-false-interacting-external-hosts-plugin

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{ \"tenantId\" : \"" + tenantId + "\",  \"resource\":  \"" + organizationURL + "\",  \"clientId\": \"" + clientId + "\", \"businessUnitId\": \"" + businessUnit.Id.ToString() + "\" }";

                streamWriter.Write(json);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                EncryptionServiceResponse response = DeserializeJSONToObject<EncryptionServiceResponse>(responseText);
                string encryptedText = response.encryptedTenantDetails;

                Entity businessUnitEntity = organizationService.Retrieve(businessUnit.LogicalName, businessUnit.Id, new ColumnSet("indskr_vanityurl"));
                string vanityURL = businessUnitEntity.GetAttributeValue<string>("indskr_vanityurl");

                Entity routingConfigurationEntity = organizationService.Retrieve(routingConfiguration.LogicalName, routingConfiguration.Id, new ColumnSet("indskr_participantform"));
                bool captureDetails = routingConfigurationEntity.Contains("indskr_participantform") ? routingConfigurationEntity.GetAttributeValue<bool>("indskr_participantform") : false;

                tracingService.Trace("IN BuildInboundURL - before getting metting URL");

                // *************** CODE TO GET MEETING URL ************************
                EncryptionServiceResponse meetingURLResponse = GetMeetingURL(businessUnit, meetingServicesURL, tenantId, organizationURL, clientId, organizationDomain);

                if (meetingURLResponse != null)
                {
                    Uri uriAddress = new Uri(meetingURLResponse.meetingUrl);
                    var meetingURL = uriAddress.GetLeftPart(UriPartial.Path);

                    tracingService.Trace("meetingURL: " + meetingURLResponse.meetingUrl);
                    tracingService.Trace("meetingURL[0]: " + meetingURL);

                    inboundURL = meetingURL;
                }

                inboundURL += "?cl=";
                inboundURL += encryptedText;
                inboundURL += "&ruleconfigid="; //the name of parameter used in the link is actually routing configuration id
                inboundURL += routingConfiguration.Id.ToString();
                inboundURL += "&capturedetails=";
                inboundURL += captureDetails.ToString().ToLower();

                if (contactId != new Guid())
                {
                    inboundURL += "&contactid=";
                    inboundURL += contactId;
                }

                return inboundURL;
            }
        }

        public EncryptionServiceResponse GetMeetingURL(EntityReference businessUnit, string meetingServicesURL, string tenantId, string organizationURL, string clientId, string organizationDomain)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(meetingServicesURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.KeepAlive = false;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{ \"tenantId\" : \"" + tenantId + "\", \"domain\":  \"" + organizationDomain + "\",  \"resource\":  \"" + organizationURL + "\",  \"clientId\": \"" + clientId + "\", \"businessUnitId\": \"" + businessUnit.Id.ToString() + "\" }";

                streamWriter.Write(json);
                streamWriter.Flush();
            }
            EncryptionServiceResponse response;
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                response = DeserializeJSONToObject<EncryptionServiceResponse>(responseText);
            }

            return response;
        }

        public LCID_Data GetLCIDs(IOrganizationService service, Guid userID)
        {
            LCID_Data lcids = new LCID_Data();

            // Build Query
            var fetchXML = String.Empty;
            fetchXML += "<fetch count='1' >";
            fetchXML += " <entity name='usersettings' >";
            fetchXML += "   <attribute name='localeid' /> ";
            fetchXML += "   <filter type='and' >";
            fetchXML += "     <condition attribute='systemuserid' operator='eq' value='" + userID + "' />";
            fetchXML += "   </filter>";
            fetchXML += "   <link-entity name='systemuser' from='systemuserid' to='systemuserid' link-type='inner' alias='User'>";
            fetchXML += "     <attribute name='fullname' />";
            fetchXML += "     <link-entity name='organization' from='organizationid' to='organizationid' alias='Org'>";
            fetchXML += "       <attribute name='localeid' />";
            fetchXML += "     </link-entity>";
            fetchXML += "   </link-entity>";
            fetchXML += " </entity>";
            fetchXML += "</fetch>";

            // Get Results
            FetchExpression fetchExpression = new FetchExpression(fetchXML);
            EntityCollection ec = service.RetrieveMultiple(fetchExpression);

            // If we have a result
            if (ec.Entities.Count > 0)
            {
                // Check User LCID
                Entity result = ec.Entities[0];
                if (result.Contains(EntityAttrs.UserSettings.LocaleId))
                {
                    lcids.UserLCID = (int)result[EntityAttrs.UserSettings.LocaleId];
                }

                // Check Org LCID
                if (result.Contains("Org.localeid"))
                {
                    //lcids.OrgLCID = GetAliasedAttributeValue<int>(result, "Org.localeid");
                    lcids.OrgLCID = result.GetAliasedAttributeValue<int>("Org.localeid");

                    // If Org LCID but no user then set userLCID = OrgLCID
                    if (lcids.UserLCID == 0)
                    {
                        lcids.UserLCID = lcids.OrgLCID;
                    }
                }
            }

            return lcids;
        }
        public void AssignTo(EntityReference assignee, Entity target, IOrganizationService orgService)
        {
            if (assignee == null || target == null)
                return;

            AssignRequest assignRequest = new AssignRequest();
            assignRequest.Assignee = assignee;
            assignRequest.Target = target.ToEntityReference();
            orgService.Execute(assignRequest);
        }

        public EntityReference getProcessor(ProcessorType contractProcessor, EntityReference businessUnit, IOrganizationService orgService)
        {
            QueryExpression queryExpression = new QueryExpression("indskr_engagementrequestprocessormapping");
            queryExpression.ColumnSet = new ColumnSet("indskr_processor");
            queryExpression.Criteria.AddCondition("indskr_processortype", ConditionOperator.Equal, (int)contractProcessor);
            queryExpression.Criteria.AddCondition("indskr_businessunit", ConditionOperator.Equal, businessUnit.Id);

            var results = orgService.RetrieveMultiple(queryExpression);
            if (results.Entities.Count > 0)
                return results.Entities[0].GetAttributeValue<EntityReference>("indskr_processor");

            return null;
        }
        public enum ProcessorType
        {
            ServiceProcessor = 548910000,
            ContractProcessor = 548910001
        }

        public void RefreshRollupField(IOrganizationService orgService, Entity targetEntity, string fieldName)
        {
            CalculateRollupFieldRequest calcRollupRequest = new CalculateRollupFieldRequest
            {
                Target = targetEntity.ToEntityReference(),
                FieldName = fieldName
            };
            CalculateRollupFieldResponse objCalcRollupResponse = (CalculateRollupFieldResponse)orgService.Execute(calcRollupRequest);
        }

        public Guid GetContactId(IOrganizationService orgService, ITracingService tracingService, EntityReference routingConfiguration)
        {
            tracingService.Trace("Routing Configuration = " + routingConfiguration.Name);
            tracingService.Trace("Routing Configuration Id = " + routingConfiguration.Id.ToString());
            Entity routingConfigurationEntity = orgService.Retrieve(routingConfiguration.LogicalName, routingConfiguration.Id, new ColumnSet(false));
            tracingService.Trace("GetRoutingRule");
            EntityReference routingRule = GetRoutingRule(orgService, tracingService, routingConfigurationEntity);
            tracingService.Trace("GetFetchXML");
            string fetchXML = GetFetchXML(orgService, tracingService, routingRule);
            tracingService.Trace("fetchXML = " + fetchXML);

            if (fetchXML == "")
                return new Guid();

            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(fetchXML);
            XmlNodeList nodeList = xdoc.GetElementsByTagName("entity");
            var entity = nodeList[0].Attributes["name"].Value;
            if (entity == "contact")
            {
                FetchExpression query = new FetchExpression(fetchXML);
                EntityCollection results = orgService.RetrieveMultiple(query);

                if (results != null && results.Entities != null && results.Entities.Count > 0)
                    return results[0].Id;
                else
                    return new Guid();
            }
            else { return new Guid(); }
        }

        public EntityReference GetRoutingRule(IOrganizationService organizationService, ITracingService tracingService, Entity routingConfigurationEntity)
        {
            QueryExpression query = new QueryExpression("indskr_routingconfigurationrule");
            query.Criteria.AddCondition(new ConditionExpression("indskr_routingconfigurationid", ConditionOperator.Equal, routingConfigurationEntity.Id));
            query.ColumnSet = new ColumnSet("indskr_routingruleid");
            EntityCollection entityCollection = organizationService.RetrieveMultiple(query);

            if (entityCollection != null && entityCollection.Entities != null && entityCollection.Entities.Count > 0)
                return (EntityReference)entityCollection[0].Attributes["indskr_routingruleid"];
            else
                return null;
        }

        public string GetFetchXML(IOrganizationService organizationService, ITracingService tracingService, EntityReference routingRule)
        {
            if (routingRule == null)
                return "";

            string fetchXML = string.Empty;
            Entity routingRuleEntity = organizationService.Retrieve(routingRule.LogicalName, routingRule.Id, new ColumnSet("indskr_query"));
            return routingRuleEntity.Attributes["indskr_query"].ToString();
        }

        public void WarnUser(IOrganizationService orgService, ITracingService tracService, Guid userId, string configItem, string errorCode = null)
        {
            try
            {
                tracService.Trace("Method: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);

                var lcids = Utilities.Inst.GetLCIDs(orgService, userId);
                var UserLCID = lcids.UserLCID;
                var OrgLCID = lcids.OrgLCID;
                //Get the error message from configuration
                iOConfigManager configMan = new iOConfigManager(orgService, tracService, OrgLCID);

                string message = configMan.GetConfiguration(configItem, UserLCID);
                if (!String.IsNullOrEmpty(errorCode))
                {
                    message = errorCode + ";;" + message;

                }
                //Show the message to the user
                throw new InvalidPluginExecutionException(message + "\n\n");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetErrorCode(string errorString)
        {
            const string errorCodePrefix = "IoExceptions";
            if (!String.IsNullOrEmpty(errorString))
            {
                if (errorString.StartsWith(errorCodePrefix))
                {
                    var codeValue = errorString.Substring(errorCodePrefix.Length);
                    return codeValue;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Calling this functions pushes the Message into the queue using the azure function.
        /// </summary>
        /// <param name="orgService"></param>
        /// <param name="tracService"></param>
        /// <param name="queueValue"></param>
        /// <param name="queueName"></param>
        public void PushMessageIntoAzureQueue(IOrganizationService orgService, ITracingService tracService, string queueValue, string queueName)
        {
            var queryioconfiguration_1_configname = "iO-DistributionQueueHelperFunctionKey";
            var queryioconfiguration_2_configname = "iO-DistributionQueueHelperURL";
            var queryioconfiguration_3_configname = "iO-DistributionQueueHelperPrefix";

            var queryIoConfiguration = new QueryExpression("indskr_ioconfiguration");

            queryIoConfiguration.ColumnSet.AddColumns("indskr_configvalue", "indskr_configname");

            var queryioconfiguration_Criteria_0 = new FilterExpression();
            queryIoConfiguration.Criteria.AddFilter(queryioconfiguration_Criteria_0);

            queryioconfiguration_Criteria_0.FilterOperator = LogicalOperator.Or;
            queryioconfiguration_Criteria_0.AddCondition("indskr_configname", ConditionOperator.Equal, queryioconfiguration_1_configname);
            queryioconfiguration_Criteria_0.AddCondition("indskr_configname", ConditionOperator.Equal, queryioconfiguration_2_configname);
            queryioconfiguration_Criteria_0.AddCondition("indskr_configname", ConditionOperator.Equal, queryioconfiguration_3_configname);

            EntityCollection result = orgService.RetrieveMultiple(queryIoConfiguration);

            string distributionQueueHelperURL = null, distributionQueueHelperFunctionKey = null, distributionQueueHelperPrefix = null;

            foreach (var entity in result.Entities)
            {
                var indskr_configValue = entity.GetAttributeValue<string>("indskr_configvalue");
                var indskr_configName = entity.GetAttributeValue<string>("indskr_configname");


                if (indskr_configName == "iO-DistributionQueueHelperURL")
                {
                    distributionQueueHelperURL = indskr_configValue;
                }
                else if (indskr_configName == "iO-DistributionQueueHelperFunctionKey")
                {
                    distributionQueueHelperFunctionKey = indskr_configValue;
                }
                else if (indskr_configName == "iO-DistributionQueueHelperPrefix")
                {
                    distributionQueueHelperPrefix = indskr_configValue;
                }
            }
            if (distributionQueueHelperURL == null || distributionQueueHelperFunctionKey == null || distributionQueueHelperPrefix == null)
            {
                throw new InvalidPluginExecutionException("Configuration Error: IO Configuration not set correctly, please validate configuration for Efax Azure Functions");
            }

            JObject body = new JObject();
            body.Add("QueueName", queueName);
            body.Add("QueueValue", queueValue);

            bool success = CallDistributionFunctionHelperWebHook(distributionQueueHelperURL, distributionQueueHelperFunctionKey, body);

            tracService.Trace($"Utilities: PushEFaxEmailIdIntoQueue: Calling CallDistributionFunctionHelperWebHook Returned {success}");

        }

        //Calling the Helper function to Move the value into the queue
        private bool CallDistributionFunctionHelperWebHook(string distributionQueueHelperURL, string distributionQueueHelperFunctionKey, JObject body)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(distributionQueueHelperURL);
                client.Timeout = new TimeSpan(0, 2, 0);
                HttpRequestMessage updateRequest = new HttpRequestMessage(new HttpMethod("POST"), distributionQueueHelperURL);
                updateRequest.Headers.Add("x-functions-key", distributionQueueHelperFunctionKey);
                updateRequest.Content = new StringContent(body.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage updateResponse = client.SendAsync(updateRequest, HttpCompletionOption.ResponseContentRead).Result;

                return updateResponse.IsSuccessStatusCode;
            }
        }

        public EntityCollection RetriveMultiple(IOrganizationService service, ITracingService tracingService, QueryExpression query)
        {
            EntityCollection resultsList = new EntityCollection();
            while (true)
            {
                EntityCollection result = service.RetrieveMultiple(query);
                resultsList.Entities.AddRange(new List<Entity>(result.Entities));
                if (result.MoreRecords)
                {
                    query.PageInfo.PageNumber++;
                    query.PageInfo.PagingCookie = result.PagingCookie;
                }
                else
                {
                    break;  // Exit loop
                }
            }


            if (resultsList.Entities.Count < 1)
            {
                tracingService.Trace("CustomerMappingHelper: RetrieveCustomersBasedOnBrick: There are no customers that are associated with brick you have selected ");
                return new EntityCollection();
            }

            return resultsList;
        }

        public SetStateResponse SetState(IOrganizationService service, EntityReference entityReference, int stateCode, int statusCode)
        {
            // Create the Request Object
            SetStateRequest state = new SetStateRequest();

            // Point the Request to the entity whose state is being changed
            state.EntityMoniker = entityReference;

            // Set the Request Object's Properties
            state.State = new OptionSetValue(stateCode);
            state.Status = new OptionSetValue(statusCode);

            // Execute the Request and return the response
            return (SetStateResponse)service.Execute(state);
        }

        public bool CreateNotes(string subject, string message, EntityReference entityReference, IOrganizationService service)
        {
            Entity Note = new Entity("annotation");
            Note["objectid"] = entityReference;
            Note["subject"] = subject;
            Note["notetext"] = message;
            var result = service.Create(Note);

            return (result != null) ? true : false;
        }

        [DataContract]
        public class EncryptionServiceResponse
        {
            [DataMember]
            public string encryptedTenantDetails { get; set; }
            [DataMember]
            public string meetingUrl { get; set; }
        }

        public class LCID_Data
        {
            public int UserLCID { get; set; }
            public int OrgLCID { get; set; }
        }

        public class UserRoles
        {
            public Boolean IoAdmin { get; set; }
            public Boolean IoManager { get; set; }
            public Boolean IoUser { get; set; }
        }
    }

    public class ExtensionEntities
    {

        /// <summary>
        /// Creates the specified extension entity record for the specified base entity and links both the base and extension records through the link fields
        /// </summary>
        /// <param name="orgService">Organization Service instance</param>
        /// <param name="baseEntity">Base entity for which the extension entitiy has to be created</param>
        /// <param name="baseEntityPrimaryField">Primary field (logical)name of the base entity</param>
        /// <param name="baseEntityLinkField">Link field (logical) name of the base entity</param>
        /// <param name="extensionEntityName">Logical name of the extension entity</param>
        /// <param name="extensionEntityPrimaryField">Primary field (logical)name of the extension entity</param>
        /// <param name="extensionEntityLinkField">Link field (logical) name of the extension entity.</param>
        public void CreateExtenstionEntityRecord(IOrganizationService orgService, Entity baseEntity, string baseEntityPrimaryField, string baseEntityLinkField,
                string extensionEntityName, string extensionEntityPrimaryField, string extensionEntityLinkField)
        {
            try
            {
                Entity newExtensionEntity = new Entity(extensionEntityName);

                //Link the base record with the new extension record
                Utilities.Inst.SetAttributeValue(newExtensionEntity, extensionEntityLinkField, new EntityReference(baseEntity.LogicalName, baseEntity.Id));

                //Set the Primary field value
                string primaryFieldValue = Utilities.Inst.GetAttributeValue<string>(baseEntity, baseEntityPrimaryField);
                Utilities.Inst.SetAttributeValue(newExtensionEntity, extensionEntityPrimaryField, primaryFieldValue);

                //Create and get the ID of the extension record
                Guid newHospitalProfileID = orgService.Create(newExtensionEntity);

                //Link the new extension record with the base record
                Entity baseEntityUpdate = new Entity(baseEntity.LogicalName);
                baseEntityUpdate.Id = baseEntity.Id;
                Utilities.Inst.SetAttributeValue(baseEntityUpdate, baseEntityLinkField, new EntityReference(extensionEntityName, newHospitalProfileID));
                orgService.Update(baseEntityUpdate);

            }
            catch (Exception)
            {

                throw;
            }
        }



        /// <summary>
        /// Updates the specified extension entity primary field.  This would be have to be done when the value of the base entity primary field changes. 
        /// </summary>
        /// <param name="orgService">Organization service instance</param>
        /// <param name="baseEntity">Base entity for which the extension entity has to be updated</param>
        /// <param name="baseEntityLinkField">Link field (logical) name of the base entity</param>
        /// <param name="extensionEntityName">Logical name of the extension entity</param>
        /// <param name="extensionEntityPrimaryField">Primary field (logical)name of the extension entity</param>
        /// <param name="extensionEntityPrimaryFieldValue">The value to be set to the primary field of the extension entity</param>
        public void UpdateExtensionEntityRecordName(IOrganizationService orgService, Entity baseEntity, string baseEntityLinkField,
                string extensionEntityName, string extensionEntityPrimaryField, string extensionEntityPrimaryFieldValue)
        {
            try
            {
                //Check if the base record has the specified linked extension Record
                EntityReference linkFieldReference = Utilities.Inst.GetAttributeValue<EntityReference>(baseEntity, baseEntityLinkField);

                if (linkFieldReference != default(EntityReference))
                {
                    //Update the extension record primary field

                    Entity extensionRecordUpdate = new Entity(extensionEntityName);
                    extensionRecordUpdate.Id = linkFieldReference.Id;
                    Utilities.Inst.SetAttributeValue(extensionRecordUpdate, extensionEntityPrimaryField, extensionEntityPrimaryFieldValue);
                    orgService.Update(extensionRecordUpdate);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



        /// <summary>
        /// Deletes the specified extension entity record
        /// </summary>
        /// <param name="orgService">Organization service instance</param>
        /// <param name="baseEntity">Base entity for which the extension entity has to be deleted</param>
        /// <param name="baseEntityLinkField">Link field (logical) name of the base entity</param>
        /// <param name="extensionEntityName">Logical name of the extension entity</param>
        public void DeleteExtensionEntityRecord(IOrganizationService orgService, Entity baseEntity, string baseEntityLinkField, string extensionEntityName)
        {
            try
            {
                EntityReference extensionEntityRef = Utilities.Inst.GetAttributeValue<EntityReference>(baseEntity, baseEntityLinkField);
                if (extensionEntityRef != default(EntityReference))
                {
                    //Delete the extension entity record
                    orgService.Delete(extensionEntityName, extensionEntityRef.Id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteExtensionEntityRecord(IOrganizationService orgService, string entityName, FilterExpression criteria)
        {
            try
            {
                QueryExpression query = new QueryExpression(entityName);
                query.Criteria = criteria;
                EntityCollection entityCollection = orgService.RetrieveMultiple(query);
                foreach (Entity entityRecord in entityCollection.Entities)
                {
                    orgService.Delete(entityRecord.LogicalName, entityRecord.Id);
                }
            }
            catch (Exception) { }
        }


    }

    public class EntityBulkOperations
    {

        /// <summary>
        /// Performs a bulk update of the specified entities
        /// </summary>
        /// <param name="service">Org Service</param>
        /// <param name="entities">Collection of entities to Update</param>
        /// <param name="continueOnError"></param>
        /// <param name="returnResponses"></param>
        /// <returns></returns>
        public ExecuteMultipleResponse BulkUpdate(IOrganizationService service, DataCollection<Entity> entities, bool continueOnError = false, bool returnResponses = true)
        {
            try
            {
                // Create an ExecuteMultipleRequest object.
                var multipleRequest = new ExecuteMultipleRequest()
                {
                    // Assign settings that define execution behavior: continue on error, return responses. 
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = continueOnError,
                        ReturnResponses = returnResponses
                    },
                    // Create an empty organization request collection.
                    Requests = new OrganizationRequestCollection()
                };

                // Add a UpdateRequest for each entity to the request collection.
                foreach (var entity in entities)
                {
                    UpdateRequest updateRequest = new UpdateRequest { Target = entity };
                    multipleRequest.Requests.Add(updateRequest);
                }

                // Execute all the requests in the request collection using a single web method call.
                ExecuteMultipleResponse multipleResponses = (ExecuteMultipleResponse)service.Execute(multipleRequest);

                return multipleResponses;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Performs a bulk delete of the specified entities
        /// </summary>
        /// <param name="service">Org Service</param>
        /// <param name="entityReferences">Collection of EntityReferences to Delete</param>
        /// <param name="continueOnError"></param>
        /// <param name="returnResponses"></param>
        /// <returns></returns>
        public ExecuteMultipleResponse BulkDelete(IOrganizationService service, DataCollection<EntityReference> entityReferences, bool continueOnError = false, bool returnResponses = true)
        {
            try
            {
                // Create an ExecuteMultipleRequest object.
                var multipleRequest = new ExecuteMultipleRequest()
                {
                    // Assign settings that define execution behavior: continue on error, return responses. 
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = continueOnError,
                        ReturnResponses = returnResponses
                    },
                    // Create an empty organization request collection.
                    Requests = new OrganizationRequestCollection()
                };

                // Add a DeleteRequest for each entity to the request collection.
                foreach (var entityRef in entityReferences)
                {
                    DeleteRequest deleteRequest = new DeleteRequest { Target = entityRef };
                    multipleRequest.Requests.Add(deleteRequest);
                }

                // Execute all the requests in the request collection using a single web method call.
                ExecuteMultipleResponse multipleResponses = (ExecuteMultipleResponse)service.Execute(multipleRequest);

                return multipleResponses;

            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Helps to bulk update an attribute with the given value on a bunch of records
        /// </summary>
        /// <param name="orgService">Organization Service</param>
        /// <param name="tracService">Tracing Service</param>
        /// <param name="fetchXML">FetchXML to retrive the target records to update</param>
        /// <param name="attribute">Name of the attribute to be updated </param>
        /// <param name="value">Value to set to the specified attribute</param>
        /// <returns></returns>
        public ExecuteMultipleResponse BulkUpdateField(IOrganizationService orgService, ITracingService tracService
            , string fetchXML, string attribute, object value)
        {
            try
            {
                tracService.Trace("Method: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);

                ExecuteMultipleResponse executeMultipleRespons = default(ExecuteMultipleResponse);

                //Get the list of target records to be updated
                tracService.Trace("Fetch XML:" + fetchXML);

                FetchExpression fetchEx = new FetchExpression(fetchXML);
                DataCollection<Entity> targetRecords = orgService.RetrieveMultiple(fetchEx).Entities;


                if (targetRecords.Count > 0)
                {

                    //Update the target field of the target records
                    foreach (Entity targetReord in targetRecords)
                        Utilities.Inst.SetAttributeValue(targetReord, attribute, value);

                    //Save the target records
                    executeMultipleRespons = Utilities.Inst.EntityBulkOprs.BulkUpdate(orgService, targetRecords, false, true);

                    if (executeMultipleRespons.IsFaulted)
                        throw new Exception("Couldn't update the target records successfully.");
                }

                return executeMultipleRespons;
            }
            catch (Exception)
            {
                throw;
            }
        }


    }

    #region Extensions Methods
    public static class ExtensionMethods
    {
        public static T GetAliasedAttributeValue<T>(this Entity entity, string attributeName)
        {
            if (entity == null)
                return default(T);

            AliasedValue fieldAliasValue = entity.GetAttributeValue<AliasedValue>(attributeName);

            if (fieldAliasValue == null)
                return default(T);

            if (fieldAliasValue.Value != null && fieldAliasValue.Value.GetType() == typeof(T))
            {
                return (T)fieldAliasValue.Value;
            }

            return default(T);
        }


        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }


    [Serializable]
    public class IOException : Exception
    {
        public int ErrorCode { get; private set; }
        //public string ErrorMessage { get;  set; }

        public IOException() { }
        public IOException(int _errorCode, string _errorMessage) : base(_errorMessage)
        {
            ErrorCode = _errorCode;
            //ErrorMessage = _errorMessage;
            base.HResult = _errorCode;
            // base..Message = _errorMessage;
        }

        //[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        //public void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        //{
        //    if (info == null)
        //        throw new ArgumentNullException("info");

        //    info.AddValue("ErrorCode", ErrorCode);
        //}
        //public string ToJSON(IOException exception)
        //{
        //    var js = new DataContractJsonSerializer(typeof(IOException));

        //    using (var ms = new MemoryStream())
        //    {
        //        js.WriteObject(ms, exception);
        //        return Encoding.UTF8.GetString(ms.ToArray());
        //    }


        //}

    }
    #endregion
}
