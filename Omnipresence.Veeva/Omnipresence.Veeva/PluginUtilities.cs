﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnipresence.Veeva.Plugins
{

    internal struct PluginMessages
    {
        public const string Create = "create";
        public const string Update = "update";
        public const string Delete = "delete";
        public const string Associate = "associate";
        public const string Disassociate = "disassociate";
        public const string SetStateDynamicEntity = "setstatedynamicentity";
        public const string IOUserShipmentAndRecall = "indskr_iOUserShipmentAndRecall";
        public const string CustomerSampleAllocation = "indskr_iOCustomerSampleAllocation";
        public const string IOContentGroupDistribution = "indskr_iOContentGroupDistribution";
        public const string IOSurveyDistribution = "indskr_iODistributeSurveyAction";
        public const string Retrieve = "retrieve";
        public const string RetrieveMultiple = "retrievemultiple";
        public const string IOReturnMatchingAppointmentAndEmails = "indskr_iOReturnMatchedEmailsAppointmentsForAScientificPlanCalledFromFlow";
    }

    internal struct PluginParameters
    {
        public const string Target = "Target";
        public const string Input = "input";
        public const string Output = "output";
        public const string Relationship = "Relationship";
        public const string RelatedEntities = "RelatedEntities";
        public const string Image = "Image";
        public const string PreImage = "PreImage";
        public const string AllocationShipmentId = "AllocationShipmentId";
        public const string IsShipping = "IsShipping";
        public const string SampleAllocationId = "SampleAllocationId";
        public const string AllocationRuleProfileId = "AllocationRuleProfileId";
        public const string ParentProductId = "ParentProductId";
        public const string IsDistribute = "IsDistribute";
        public const string Response = "Response";
        public const string ContentGroupId = "ContentGroupId";
        public const string IsRedistribute = "IsRedistribute";
        public const string SurveyId = "SurveyId";
        public const string TeamId = "TeamId";
        public const string EmailId = "EmailId";
        public const string EntityMoniker = "EntityMoniker";
        public const string Status = "Status";
        public const string PostImage = "PostImage";
        public const string Emails = "Emails";
        public const string Appointments = "Appointments";
    }

    internal struct PluginImages
    {
        public const string PreImage = "PreImage";
        public const string PostImage = "PostImage";
    }


    public class PluginUtilities
    {

        #region Singleton Implementation

        //Hide the default constructor
        private PluginUtilities() { }

        private static PluginUtilities _inst = default(PluginUtilities);
        public static PluginUtilities Inst
        {
            get
            {
                if (_inst == null)
                    _inst = new PluginUtilities();

                return _inst;
            }
        }

        #endregion


        public Entity GetContextEntity(IPluginExecutionContext PluginContext, IOrganizationService OrgService)
        {
            return PluginContext.InputParameters["Target"] as Entity;
        }
        public Entity GetServiceEntity(IPluginExecutionContext PluginContext, IOrganizationService OrgService, string entityName, Guid entityId, ColumnSet columnSet)
        {
            try
            {
                return OrgService.Retrieve(entityName, entityId, columnSet);
            }
            catch
            {
                return null;
            }
        }

    }

}
